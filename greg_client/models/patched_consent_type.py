import datetime
import json
from typing import TYPE_CHECKING, Any, Dict, List, Tuple, Type, TypeVar, Union

from attrs import define as _attrs_define
from attrs import field as _attrs_field
from dateutil.parser import isoparse

from ..types import UNSET, Unset

if TYPE_CHECKING:
    from ..models.consent_choice import ConsentChoice


T = TypeVar("T", bound="PatchedConsentType")


@_attrs_define
class PatchedConsentType:
    """
    Attributes:
        id (Union[Unset, int]):
        identifier (Union[Unset, str]):
        name_en (Union[Unset, str]):
        name_nb (Union[Unset, str]):
        name_nn (Union[Unset, str]):
        description_en (Union[Unset, str]):
        description_nb (Union[Unset, str]):
        description_nn (Union[Unset, str]):
        mandatory (Union[Unset, bool]):
        user_allowed_to_change (Union[Unset, bool]):
        valid_from (Union[Unset, datetime.date]):
        choices (Union[Unset, List['ConsentChoice']]):
        created (Union[Unset, datetime.datetime]):
        updated (Union[Unset, datetime.datetime]):
    """

    id: Union[Unset, int] = UNSET
    identifier: Union[Unset, str] = UNSET
    name_en: Union[Unset, str] = UNSET
    name_nb: Union[Unset, str] = UNSET
    name_nn: Union[Unset, str] = UNSET
    description_en: Union[Unset, str] = UNSET
    description_nb: Union[Unset, str] = UNSET
    description_nn: Union[Unset, str] = UNSET
    mandatory: Union[Unset, bool] = UNSET
    user_allowed_to_change: Union[Unset, bool] = UNSET
    valid_from: Union[Unset, datetime.date] = UNSET
    choices: Union[Unset, List["ConsentChoice"]] = UNSET
    created: Union[Unset, datetime.datetime] = UNSET
    updated: Union[Unset, datetime.datetime] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        id = self.id

        identifier = self.identifier

        name_en = self.name_en

        name_nb = self.name_nb

        name_nn = self.name_nn

        description_en = self.description_en

        description_nb = self.description_nb

        description_nn = self.description_nn

        mandatory = self.mandatory

        user_allowed_to_change = self.user_allowed_to_change

        valid_from: Union[Unset, str] = UNSET
        if not isinstance(self.valid_from, Unset):
            valid_from = self.valid_from.isoformat()

        choices: Union[Unset, List[Dict[str, Any]]] = UNSET
        if not isinstance(self.choices, Unset):
            choices = []
            for choices_item_data in self.choices:
                choices_item = choices_item_data.to_dict()
                choices.append(choices_item)

        created: Union[Unset, str] = UNSET
        if not isinstance(self.created, Unset):
            created = self.created.isoformat()

        updated: Union[Unset, str] = UNSET
        if not isinstance(self.updated, Unset):
            updated = self.updated.isoformat()

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update({})
        if id is not UNSET:
            field_dict["id"] = id
        if identifier is not UNSET:
            field_dict["identifier"] = identifier
        if name_en is not UNSET:
            field_dict["name_en"] = name_en
        if name_nb is not UNSET:
            field_dict["name_nb"] = name_nb
        if name_nn is not UNSET:
            field_dict["name_nn"] = name_nn
        if description_en is not UNSET:
            field_dict["description_en"] = description_en
        if description_nb is not UNSET:
            field_dict["description_nb"] = description_nb
        if description_nn is not UNSET:
            field_dict["description_nn"] = description_nn
        if mandatory is not UNSET:
            field_dict["mandatory"] = mandatory
        if user_allowed_to_change is not UNSET:
            field_dict["user_allowed_to_change"] = user_allowed_to_change
        if valid_from is not UNSET:
            field_dict["valid_from"] = valid_from
        if choices is not UNSET:
            field_dict["choices"] = choices
        if created is not UNSET:
            field_dict["created"] = created
        if updated is not UNSET:
            field_dict["updated"] = updated

        return field_dict

    def to_multipart(self) -> Dict[str, Any]:
        id = self.id if isinstance(self.id, Unset) else (None, str(self.id).encode(), "text/plain")

        identifier = (
            self.identifier
            if isinstance(self.identifier, Unset)
            else (None, str(self.identifier).encode(), "text/plain")
        )

        name_en = self.name_en if isinstance(self.name_en, Unset) else (None, str(self.name_en).encode(), "text/plain")

        name_nb = self.name_nb if isinstance(self.name_nb, Unset) else (None, str(self.name_nb).encode(), "text/plain")

        name_nn = self.name_nn if isinstance(self.name_nn, Unset) else (None, str(self.name_nn).encode(), "text/plain")

        description_en = (
            self.description_en
            if isinstance(self.description_en, Unset)
            else (None, str(self.description_en).encode(), "text/plain")
        )

        description_nb = (
            self.description_nb
            if isinstance(self.description_nb, Unset)
            else (None, str(self.description_nb).encode(), "text/plain")
        )

        description_nn = (
            self.description_nn
            if isinstance(self.description_nn, Unset)
            else (None, str(self.description_nn).encode(), "text/plain")
        )

        mandatory = (
            self.mandatory if isinstance(self.mandatory, Unset) else (None, str(self.mandatory).encode(), "text/plain")
        )

        user_allowed_to_change = (
            self.user_allowed_to_change
            if isinstance(self.user_allowed_to_change, Unset)
            else (None, str(self.user_allowed_to_change).encode(), "text/plain")
        )

        valid_from: Union[Unset, bytes] = UNSET
        if not isinstance(self.valid_from, Unset):
            valid_from = self.valid_from.isoformat().encode()

        choices: Union[Unset, Tuple[None, bytes, str]] = UNSET
        if not isinstance(self.choices, Unset):
            _temp_choices = []
            for choices_item_data in self.choices:
                choices_item = choices_item_data.to_dict()
                _temp_choices.append(choices_item)
            choices = (None, json.dumps(_temp_choices).encode(), "application/json")

        created: Union[Unset, bytes] = UNSET
        if not isinstance(self.created, Unset):
            created = self.created.isoformat().encode()

        updated: Union[Unset, bytes] = UNSET
        if not isinstance(self.updated, Unset):
            updated = self.updated.isoformat().encode()

        field_dict: Dict[str, Any] = {}
        field_dict.update(
            {key: (None, str(value).encode(), "text/plain") for key, value in self.additional_properties.items()}
        )
        field_dict.update({})
        if id is not UNSET:
            field_dict["id"] = id
        if identifier is not UNSET:
            field_dict["identifier"] = identifier
        if name_en is not UNSET:
            field_dict["name_en"] = name_en
        if name_nb is not UNSET:
            field_dict["name_nb"] = name_nb
        if name_nn is not UNSET:
            field_dict["name_nn"] = name_nn
        if description_en is not UNSET:
            field_dict["description_en"] = description_en
        if description_nb is not UNSET:
            field_dict["description_nb"] = description_nb
        if description_nn is not UNSET:
            field_dict["description_nn"] = description_nn
        if mandatory is not UNSET:
            field_dict["mandatory"] = mandatory
        if user_allowed_to_change is not UNSET:
            field_dict["user_allowed_to_change"] = user_allowed_to_change
        if valid_from is not UNSET:
            field_dict["valid_from"] = valid_from
        if choices is not UNSET:
            field_dict["choices"] = choices
        if created is not UNSET:
            field_dict["created"] = created
        if updated is not UNSET:
            field_dict["updated"] = updated

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.consent_choice import ConsentChoice

        d = src_dict.copy()
        id = d.pop("id", UNSET)

        identifier = d.pop("identifier", UNSET)

        name_en = d.pop("name_en", UNSET)

        name_nb = d.pop("name_nb", UNSET)

        name_nn = d.pop("name_nn", UNSET)

        description_en = d.pop("description_en", UNSET)

        description_nb = d.pop("description_nb", UNSET)

        description_nn = d.pop("description_nn", UNSET)

        mandatory = d.pop("mandatory", UNSET)

        user_allowed_to_change = d.pop("user_allowed_to_change", UNSET)

        _valid_from = d.pop("valid_from", UNSET)
        valid_from: Union[Unset, datetime.date]
        if isinstance(_valid_from, Unset):
            valid_from = UNSET
        else:
            valid_from = isoparse(_valid_from).date()

        choices = []
        _choices = d.pop("choices", UNSET)
        for choices_item_data in _choices or []:
            choices_item = ConsentChoice.from_dict(choices_item_data)

            choices.append(choices_item)

        _created = d.pop("created", UNSET)
        created: Union[Unset, datetime.datetime]
        if isinstance(_created, Unset):
            created = UNSET
        else:
            created = isoparse(_created)

        _updated = d.pop("updated", UNSET)
        updated: Union[Unset, datetime.datetime]
        if isinstance(_updated, Unset):
            updated = UNSET
        else:
            updated = isoparse(_updated)

        patched_consent_type = cls(
            id=id,
            identifier=identifier,
            name_en=name_en,
            name_nb=name_nb,
            name_nn=name_nn,
            description_en=description_en,
            description_nb=description_nb,
            description_nn=description_nn,
            mandatory=mandatory,
            user_allowed_to_change=user_allowed_to_change,
            valid_from=valid_from,
            choices=choices,
            created=created,
            updated=updated,
        )

        patched_consent_type.additional_properties = d
        return patched_consent_type

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
