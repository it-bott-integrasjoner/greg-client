import datetime
from typing import Any, Dict, List, Type, TypeVar, Union, cast

from attrs import define as _attrs_define
from attrs import field as _attrs_field
from dateutil.parser import isoparse

from ..models.blank_enum import BlankEnum
from ..models.type_enum import TypeEnum
from ..models.verified_enum import VerifiedEnum
from ..types import UNSET, Unset

T = TypeVar("T", bound="Identity")


@_attrs_define
class Identity:
    """
    Attributes:
        id (int):
        created (datetime.datetime):
        updated (datetime.datetime):
        type (TypeEnum):
        source (str):
        value (str):
        person (int):
        verified (Union[BlankEnum, Unset, VerifiedEnum]):
        verified_at (Union[None, Unset, datetime.datetime]):
        invalid (Union[None, Unset, bool]):
        verified_by (Union[None, Unset, int]):
    """

    id: int
    created: datetime.datetime
    updated: datetime.datetime
    type: TypeEnum
    source: str
    value: str
    person: int
    verified: Union[BlankEnum, Unset, VerifiedEnum] = UNSET
    verified_at: Union[None, Unset, datetime.datetime] = UNSET
    invalid: Union[None, Unset, bool] = UNSET
    verified_by: Union[None, Unset, int] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        id = self.id

        created = self.created.isoformat()

        updated = self.updated.isoformat()

        type = self.type.value

        source = self.source

        value = self.value

        person = self.person

        verified: Union[Unset, str]
        if isinstance(self.verified, Unset):
            verified = UNSET
        elif isinstance(self.verified, VerifiedEnum):
            verified = self.verified.value
        else:
            verified = self.verified.value

        verified_at: Union[None, Unset, str]
        if isinstance(self.verified_at, Unset):
            verified_at = UNSET
        elif isinstance(self.verified_at, datetime.datetime):
            verified_at = self.verified_at.isoformat()
        else:
            verified_at = self.verified_at

        invalid: Union[None, Unset, bool]
        if isinstance(self.invalid, Unset):
            invalid = UNSET
        else:
            invalid = self.invalid

        verified_by: Union[None, Unset, int]
        if isinstance(self.verified_by, Unset):
            verified_by = UNSET
        else:
            verified_by = self.verified_by

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "id": id,
                "created": created,
                "updated": updated,
                "type": type,
                "source": source,
                "value": value,
                "person": person,
            }
        )
        if verified is not UNSET:
            field_dict["verified"] = verified
        if verified_at is not UNSET:
            field_dict["verified_at"] = verified_at
        if invalid is not UNSET:
            field_dict["invalid"] = invalid
        if verified_by is not UNSET:
            field_dict["verified_by"] = verified_by

        return field_dict

    def to_multipart(self) -> Dict[str, Any]:
        id = self.id if isinstance(self.id, Unset) else (None, str(self.id).encode(), "text/plain")

        created = self.created.isoformat().encode()

        updated = self.updated.isoformat().encode()

        type = (None, str(self.type.value).encode(), "text/plain")

        source = self.source if isinstance(self.source, Unset) else (None, str(self.source).encode(), "text/plain")

        value = self.value if isinstance(self.value, Unset) else (None, str(self.value).encode(), "text/plain")

        person = self.person if isinstance(self.person, Unset) else (None, str(self.person).encode(), "text/plain")

        verified: Union[BlankEnum, Unset, VerifiedEnum]
        if isinstance(self.verified, Unset):
            verified = UNSET
        elif isinstance(self.verified, VerifiedEnum):
            verified = (None, str(self.verified.value).encode(), "text/plain")
        else:
            verified = (None, str(self.verified.value).encode(), "text/plain")

        verified_at: Union[None, Unset, datetime.datetime]
        if isinstance(self.verified_at, Unset):
            verified_at = UNSET
        elif isinstance(self.verified_at, datetime.datetime):
            verified_at = self.verified_at.isoformat().encode()
        else:
            verified_at = self.verified_at

        invalid: Union[None, Unset, bool]
        if isinstance(self.invalid, Unset):
            invalid = UNSET
        else:
            invalid = self.invalid

        verified_by: Union[None, Unset, int]
        if isinstance(self.verified_by, Unset):
            verified_by = UNSET
        else:
            verified_by = self.verified_by

        field_dict: Dict[str, Any] = {}
        field_dict.update(
            {key: (None, str(value).encode(), "text/plain") for key, value in self.additional_properties.items()}
        )
        field_dict.update(
            {
                "id": id,
                "created": created,
                "updated": updated,
                "type": type,
                "source": source,
                "value": value,
                "person": person,
            }
        )
        if verified is not UNSET:
            field_dict["verified"] = verified
        if verified_at is not UNSET:
            field_dict["verified_at"] = verified_at
        if invalid is not UNSET:
            field_dict["invalid"] = invalid
        if verified_by is not UNSET:
            field_dict["verified_by"] = verified_by

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        d = src_dict.copy()
        id = d.pop("id")

        created = isoparse(d.pop("created"))

        updated = isoparse(d.pop("updated"))

        type = TypeEnum(d.pop("type"))

        source = d.pop("source")

        value = d.pop("value")

        person = d.pop("person")

        def _parse_verified(data: object) -> Union[BlankEnum, Unset, VerifiedEnum]:
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, str):
                    raise TypeError()
                verified_type_0 = VerifiedEnum(data)

                return verified_type_0
            except:  # noqa: E722
                pass
            if not isinstance(data, str):
                raise TypeError()
            verified_type_1 = BlankEnum(data)

            return verified_type_1

        verified = _parse_verified(d.pop("verified", UNSET))

        def _parse_verified_at(data: object) -> Union[None, Unset, datetime.datetime]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, str):
                    raise TypeError()
                verified_at_type_0 = isoparse(data)

                return verified_at_type_0
            except:  # noqa: E722
                pass
            return cast(Union[None, Unset, datetime.datetime], data)

        verified_at = _parse_verified_at(d.pop("verified_at", UNSET))

        def _parse_invalid(data: object) -> Union[None, Unset, bool]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, bool], data)

        invalid = _parse_invalid(d.pop("invalid", UNSET))

        def _parse_verified_by(data: object) -> Union[None, Unset, int]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, int], data)

        verified_by = _parse_verified_by(d.pop("verified_by", UNSET))

        identity = cls(
            id=id,
            created=created,
            updated=updated,
            type=type,
            source=source,
            value=value,
            person=person,
            verified=verified,
            verified_at=verified_at,
            invalid=invalid,
            verified_by=verified_by,
        )

        identity.additional_properties = d
        return identity

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
