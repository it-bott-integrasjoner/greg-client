from http import HTTPStatus
from typing import Any, Dict, Optional, Union

import httpx

from ... import errors
from ...client import AuthenticatedClient, Client
from ...models.paginated_person_list import PaginatedPersonList
from ...types import UNSET, Response, Unset


def _get_kwargs(
    sponsor_id: int,
    *,
    cursor: Union[Unset, str] = UNSET,
    page_size: Union[Unset, int] = UNSET,
) -> Dict[str, Any]:
    params: Dict[str, Any] = {}

    params["cursor"] = cursor

    params["page_size"] = page_size

    params = {k: v for k, v in params.items() if v is not UNSET and v is not None}

    _kwargs: Dict[str, Any] = {
        "method": "get",
        "url": f"/v1/sponsors/{sponsor_id}/guests",
        "params": params,
    }

    return _kwargs


def _parse_response(
    *, client: Union[AuthenticatedClient, Client], response: httpx.Response
) -> Optional[PaginatedPersonList]:
    if response.status_code == HTTPStatus.OK:
        response_200 = PaginatedPersonList.from_dict(response.json())

        return response_200
    if client.raise_on_unexpected_status:
        raise errors.UnexpectedStatus(response.status_code, response.content)
    else:
        return None


def _build_response(
    *, client: Union[AuthenticatedClient, Client], response: httpx.Response
) -> Response[PaginatedPersonList]:
    return Response(
        status_code=HTTPStatus(response.status_code),
        content=response.content,
        headers=response.headers,
        parsed=_parse_response(client=client, response=response),
    )


def sync_detailed(
    sponsor_id: int,
    *,
    client: AuthenticatedClient,
    cursor: Union[Unset, str] = UNSET,
    page_size: Union[Unset, int] = UNSET,
) -> Response[PaginatedPersonList]:
    """
    Args:
        sponsor_id (int):
        cursor (Union[Unset, str]):
        page_size (Union[Unset, int]):

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        Response[PaginatedPersonList]
    """

    kwargs = _get_kwargs(
        sponsor_id=sponsor_id,
        cursor=cursor,
        page_size=page_size,
    )

    response = client.get_httpx_client().request(
        **kwargs,
    )

    return _build_response(client=client, response=response)


def sync(
    sponsor_id: int,
    *,
    client: AuthenticatedClient,
    cursor: Union[Unset, str] = UNSET,
    page_size: Union[Unset, int] = UNSET,
) -> Optional[PaginatedPersonList]:
    """
    Args:
        sponsor_id (int):
        cursor (Union[Unset, str]):
        page_size (Union[Unset, int]):

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        PaginatedPersonList
    """

    return sync_detailed(
        sponsor_id=sponsor_id,
        client=client,
        cursor=cursor,
        page_size=page_size,
    ).parsed


async def asyncio_detailed(
    sponsor_id: int,
    *,
    client: AuthenticatedClient,
    cursor: Union[Unset, str] = UNSET,
    page_size: Union[Unset, int] = UNSET,
) -> Response[PaginatedPersonList]:
    """
    Args:
        sponsor_id (int):
        cursor (Union[Unset, str]):
        page_size (Union[Unset, int]):

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        Response[PaginatedPersonList]
    """

    kwargs = _get_kwargs(
        sponsor_id=sponsor_id,
        cursor=cursor,
        page_size=page_size,
    )

    response = await client.get_async_httpx_client().request(**kwargs)

    return _build_response(client=client, response=response)


async def asyncio(
    sponsor_id: int,
    *,
    client: AuthenticatedClient,
    cursor: Union[Unset, str] = UNSET,
    page_size: Union[Unset, int] = UNSET,
) -> Optional[PaginatedPersonList]:
    """
    Args:
        sponsor_id (int):
        cursor (Union[Unset, str]):
        page_size (Union[Unset, int]):

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        PaginatedPersonList
    """

    return (
        await asyncio_detailed(
            sponsor_id=sponsor_id,
            client=client,
            cursor=cursor,
            page_size=page_size,
        )
    ).parsed
