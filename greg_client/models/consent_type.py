import datetime
import json
from typing import TYPE_CHECKING, Any, Dict, List, Type, TypeVar, Union

from attrs import define as _attrs_define
from attrs import field as _attrs_field
from dateutil.parser import isoparse

from ..types import UNSET, Unset

if TYPE_CHECKING:
    from ..models.consent_choice import ConsentChoice


T = TypeVar("T", bound="ConsentType")


@_attrs_define
class ConsentType:
    """
    Attributes:
        id (int):
        identifier (str):
        name_en (str):
        name_nb (str):
        name_nn (str):
        description_en (str):
        description_nb (str):
        description_nn (str):
        user_allowed_to_change (bool):
        choices (List['ConsentChoice']):
        created (datetime.datetime):
        updated (datetime.datetime):
        mandatory (Union[Unset, bool]):
        valid_from (Union[Unset, datetime.date]):
    """

    id: int
    identifier: str
    name_en: str
    name_nb: str
    name_nn: str
    description_en: str
    description_nb: str
    description_nn: str
    user_allowed_to_change: bool
    choices: List["ConsentChoice"]
    created: datetime.datetime
    updated: datetime.datetime
    mandatory: Union[Unset, bool] = UNSET
    valid_from: Union[Unset, datetime.date] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        id = self.id

        identifier = self.identifier

        name_en = self.name_en

        name_nb = self.name_nb

        name_nn = self.name_nn

        description_en = self.description_en

        description_nb = self.description_nb

        description_nn = self.description_nn

        user_allowed_to_change = self.user_allowed_to_change

        choices = []
        for choices_item_data in self.choices:
            choices_item = choices_item_data.to_dict()
            choices.append(choices_item)

        created = self.created.isoformat()

        updated = self.updated.isoformat()

        mandatory = self.mandatory

        valid_from: Union[Unset, str] = UNSET
        if not isinstance(self.valid_from, Unset):
            valid_from = self.valid_from.isoformat()

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "id": id,
                "identifier": identifier,
                "name_en": name_en,
                "name_nb": name_nb,
                "name_nn": name_nn,
                "description_en": description_en,
                "description_nb": description_nb,
                "description_nn": description_nn,
                "user_allowed_to_change": user_allowed_to_change,
                "choices": choices,
                "created": created,
                "updated": updated,
            }
        )
        if mandatory is not UNSET:
            field_dict["mandatory"] = mandatory
        if valid_from is not UNSET:
            field_dict["valid_from"] = valid_from

        return field_dict

    def to_multipart(self) -> Dict[str, Any]:
        id = self.id if isinstance(self.id, Unset) else (None, str(self.id).encode(), "text/plain")

        identifier = (
            self.identifier
            if isinstance(self.identifier, Unset)
            else (None, str(self.identifier).encode(), "text/plain")
        )

        name_en = self.name_en if isinstance(self.name_en, Unset) else (None, str(self.name_en).encode(), "text/plain")

        name_nb = self.name_nb if isinstance(self.name_nb, Unset) else (None, str(self.name_nb).encode(), "text/plain")

        name_nn = self.name_nn if isinstance(self.name_nn, Unset) else (None, str(self.name_nn).encode(), "text/plain")

        description_en = (
            self.description_en
            if isinstance(self.description_en, Unset)
            else (None, str(self.description_en).encode(), "text/plain")
        )

        description_nb = (
            self.description_nb
            if isinstance(self.description_nb, Unset)
            else (None, str(self.description_nb).encode(), "text/plain")
        )

        description_nn = (
            self.description_nn
            if isinstance(self.description_nn, Unset)
            else (None, str(self.description_nn).encode(), "text/plain")
        )

        user_allowed_to_change = (
            self.user_allowed_to_change
            if isinstance(self.user_allowed_to_change, Unset)
            else (None, str(self.user_allowed_to_change).encode(), "text/plain")
        )

        _temp_choices = []
        for choices_item_data in self.choices:
            choices_item = choices_item_data.to_dict()
            _temp_choices.append(choices_item)
        choices = (None, json.dumps(_temp_choices).encode(), "application/json")

        created = self.created.isoformat().encode()

        updated = self.updated.isoformat().encode()

        mandatory = (
            self.mandatory if isinstance(self.mandatory, Unset) else (None, str(self.mandatory).encode(), "text/plain")
        )

        valid_from: Union[Unset, bytes] = UNSET
        if not isinstance(self.valid_from, Unset):
            valid_from = self.valid_from.isoformat().encode()

        field_dict: Dict[str, Any] = {}
        field_dict.update(
            {key: (None, str(value).encode(), "text/plain") for key, value in self.additional_properties.items()}
        )
        field_dict.update(
            {
                "id": id,
                "identifier": identifier,
                "name_en": name_en,
                "name_nb": name_nb,
                "name_nn": name_nn,
                "description_en": description_en,
                "description_nb": description_nb,
                "description_nn": description_nn,
                "user_allowed_to_change": user_allowed_to_change,
                "choices": choices,
                "created": created,
                "updated": updated,
            }
        )
        if mandatory is not UNSET:
            field_dict["mandatory"] = mandatory
        if valid_from is not UNSET:
            field_dict["valid_from"] = valid_from

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.consent_choice import ConsentChoice

        d = src_dict.copy()
        id = d.pop("id")

        identifier = d.pop("identifier")

        name_en = d.pop("name_en")

        name_nb = d.pop("name_nb")

        name_nn = d.pop("name_nn")

        description_en = d.pop("description_en")

        description_nb = d.pop("description_nb")

        description_nn = d.pop("description_nn")

        user_allowed_to_change = d.pop("user_allowed_to_change")

        choices = []
        _choices = d.pop("choices")
        for choices_item_data in _choices:
            choices_item = ConsentChoice.from_dict(choices_item_data)

            choices.append(choices_item)

        created = isoparse(d.pop("created"))

        updated = isoparse(d.pop("updated"))

        mandatory = d.pop("mandatory", UNSET)

        _valid_from = d.pop("valid_from", UNSET)
        valid_from: Union[Unset, datetime.date]
        if isinstance(_valid_from, Unset):
            valid_from = UNSET
        else:
            valid_from = isoparse(_valid_from).date()

        consent_type = cls(
            id=id,
            identifier=identifier,
            name_en=name_en,
            name_nb=name_nb,
            name_nn=name_nn,
            description_en=description_en,
            description_nb=description_nb,
            description_nn=description_nn,
            user_allowed_to_change=user_allowed_to_change,
            choices=choices,
            created=created,
            updated=updated,
            mandatory=mandatory,
            valid_from=valid_from,
        )

        consent_type.additional_properties = d
        return consent_type

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
