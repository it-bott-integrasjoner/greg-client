import json
from typing import TYPE_CHECKING, Any, Dict, List, Type, TypeVar

from attrs import define as _attrs_define
from attrs import field as _attrs_field

from ..types import Unset

if TYPE_CHECKING:
    from ..models.role_invitation import RoleInvitation


T = TypeVar("T", bound="Invitation")


@_attrs_define
class Invitation:
    """
    Attributes:
        id (int):
        first_name (str):
        last_name (str):
        email (str):
        role (RoleInvitation): Serializer used when creating an invitation
        sponsor (str):
    """

    id: int
    first_name: str
    last_name: str
    email: str
    role: "RoleInvitation"
    sponsor: str
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        id = self.id

        first_name = self.first_name

        last_name = self.last_name

        email = self.email

        role = self.role.to_dict()

        sponsor = self.sponsor

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "id": id,
                "first_name": first_name,
                "last_name": last_name,
                "email": email,
                "role": role,
                "sponsor": sponsor,
            }
        )

        return field_dict

    def to_multipart(self) -> Dict[str, Any]:
        id = self.id if isinstance(self.id, Unset) else (None, str(self.id).encode(), "text/plain")

        first_name = (
            self.first_name
            if isinstance(self.first_name, Unset)
            else (None, str(self.first_name).encode(), "text/plain")
        )

        last_name = (
            self.last_name if isinstance(self.last_name, Unset) else (None, str(self.last_name).encode(), "text/plain")
        )

        email = self.email if isinstance(self.email, Unset) else (None, str(self.email).encode(), "text/plain")

        role = (None, json.dumps(self.role.to_dict()).encode(), "application/json")

        sponsor = self.sponsor if isinstance(self.sponsor, Unset) else (None, str(self.sponsor).encode(), "text/plain")

        field_dict: Dict[str, Any] = {}
        field_dict.update(
            {key: (None, str(value).encode(), "text/plain") for key, value in self.additional_properties.items()}
        )
        field_dict.update(
            {
                "id": id,
                "first_name": first_name,
                "last_name": last_name,
                "email": email,
                "role": role,
                "sponsor": sponsor,
            }
        )

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.role_invitation import RoleInvitation

        d = src_dict.copy()
        id = d.pop("id")

        first_name = d.pop("first_name")

        last_name = d.pop("last_name")

        email = d.pop("email")

        role = RoleInvitation.from_dict(d.pop("role"))

        sponsor = d.pop("sponsor")

        invitation = cls(
            id=id,
            first_name=first_name,
            last_name=last_name,
            email=email,
            role=role,
            sponsor=sponsor,
        )

        invitation.additional_properties = d
        return invitation

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
