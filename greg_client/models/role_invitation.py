import datetime
from typing import Any, Dict, List, Type, TypeVar, Union, cast

from attrs import define as _attrs_define
from attrs import field as _attrs_field
from dateutil.parser import isoparse

from ..types import UNSET, Unset

T = TypeVar("T", bound="RoleInvitation")


@_attrs_define
class RoleInvitation:
    """Serializer used when creating an invitation

    Attributes:
        id (int):
        end_date (datetime.date):
        orgunit (int):
        type (str):
        start_date (Union[None, Unset, datetime.date]):
        comments (Union[Unset, str]):
        contact_person_unit (Union[Unset, str]):
    """

    id: int
    end_date: datetime.date
    orgunit: int
    type: str
    start_date: Union[None, Unset, datetime.date] = UNSET
    comments: Union[Unset, str] = UNSET
    contact_person_unit: Union[Unset, str] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        id = self.id

        end_date = self.end_date.isoformat()

        orgunit = self.orgunit

        type = self.type

        start_date: Union[None, Unset, str]
        if isinstance(self.start_date, Unset):
            start_date = UNSET
        elif isinstance(self.start_date, datetime.date):
            start_date = self.start_date.isoformat()
        else:
            start_date = self.start_date

        comments = self.comments

        contact_person_unit = self.contact_person_unit

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "id": id,
                "end_date": end_date,
                "orgunit": orgunit,
                "type": type,
            }
        )
        if start_date is not UNSET:
            field_dict["start_date"] = start_date
        if comments is not UNSET:
            field_dict["comments"] = comments
        if contact_person_unit is not UNSET:
            field_dict["contact_person_unit"] = contact_person_unit

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        d = src_dict.copy()
        id = d.pop("id")

        end_date = isoparse(d.pop("end_date")).date()

        orgunit = d.pop("orgunit")

        type = d.pop("type")

        def _parse_start_date(data: object) -> Union[None, Unset, datetime.date]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, str):
                    raise TypeError()
                start_date_type_0 = isoparse(data).date()

                return start_date_type_0
            except:  # noqa: E722
                pass
            return cast(Union[None, Unset, datetime.date], data)

        start_date = _parse_start_date(d.pop("start_date", UNSET))

        comments = d.pop("comments", UNSET)

        contact_person_unit = d.pop("contact_person_unit", UNSET)

        role_invitation = cls(
            id=id,
            end_date=end_date,
            orgunit=orgunit,
            type=type,
            start_date=start_date,
            comments=comments,
            contact_person_unit=contact_person_unit,
        )

        role_invitation.additional_properties = d
        return role_invitation

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
