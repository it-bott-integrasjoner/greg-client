from enum import Enum


class TypeEnum(str, Enum):
    FEIDE_EMAIL = "feide_email"
    FEIDE_ID = "feide_id"
    MIGRATION_ID = "migration_id"
    NATIONAL_ID_CARD_NUMBER = "national_id_card_number"
    NORWEGIAN_NATIONAL_ID_CARD_NUMBER = "norwegian_national_id_card_number"
    NORWEGIAN_NATIONAL_ID_NUMBER = "norwegian_national_id_number"
    PASSPORT_NUMBER = "passport_number"
    PRIVATE_EMAIL = "private_email"
    PRIVATE_MOBILE = "private_mobile"

    def __str__(self) -> str:
        return str(self.value)
