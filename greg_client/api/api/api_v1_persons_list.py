from http import HTTPStatus
from typing import Any, Dict, Optional, Union

import httpx

from ... import errors
from ...client import AuthenticatedClient, Client
from ...models.paginated_person_list import PaginatedPersonList
from ...types import UNSET, Response, Unset


def _get_kwargs(
    *,
    active: Union[Unset, bool] = UNSET,
    cursor: Union[Unset, str] = UNSET,
    first_name: Union[Unset, str] = UNSET,
    last_name: Union[Unset, str] = UNSET,
    page_size: Union[Unset, int] = UNSET,
    role_type: Union[Unset, str] = UNSET,
    verified: Union[Unset, bool] = UNSET,
) -> Dict[str, Any]:
    params: Dict[str, Any] = {}

    params["active"] = active

    params["cursor"] = cursor

    params["first_name"] = first_name

    params["last_name"] = last_name

    params["page_size"] = page_size

    params["role_type"] = role_type

    params["verified"] = verified

    params = {k: v for k, v in params.items() if v is not UNSET and v is not None}

    _kwargs: Dict[str, Any] = {
        "method": "get",
        "url": "/v1/persons",
        "params": params,
    }

    return _kwargs


def _parse_response(
    *, client: Union[AuthenticatedClient, Client], response: httpx.Response
) -> Optional[PaginatedPersonList]:
    if response.status_code == HTTPStatus.OK:
        response_200 = PaginatedPersonList.from_dict(response.json())

        return response_200
    if client.raise_on_unexpected_status:
        raise errors.UnexpectedStatus(response.status_code, response.content)
    else:
        return None


def _build_response(
    *, client: Union[AuthenticatedClient, Client], response: httpx.Response
) -> Response[PaginatedPersonList]:
    return Response(
        status_code=HTTPStatus(response.status_code),
        content=response.content,
        headers=response.headers,
        parsed=_parse_response(client=client, response=response),
    )


def sync_detailed(
    *,
    client: AuthenticatedClient,
    active: Union[Unset, bool] = UNSET,
    cursor: Union[Unset, str] = UNSET,
    first_name: Union[Unset, str] = UNSET,
    last_name: Union[Unset, str] = UNSET,
    page_size: Union[Unset, int] = UNSET,
    role_type: Union[Unset, str] = UNSET,
    verified: Union[Unset, bool] = UNSET,
) -> Response[PaginatedPersonList]:
    """Person API

    Args:
        active (Union[Unset, bool]):
        cursor (Union[Unset, str]):
        first_name (Union[Unset, str]):
        last_name (Union[Unset, str]):
        page_size (Union[Unset, int]):
        role_type (Union[Unset, str]):
        verified (Union[Unset, bool]):

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        Response[PaginatedPersonList]
    """

    kwargs = _get_kwargs(
        active=active,
        cursor=cursor,
        first_name=first_name,
        last_name=last_name,
        page_size=page_size,
        role_type=role_type,
        verified=verified,
    )

    response = client.get_httpx_client().request(
        **kwargs,
    )

    return _build_response(client=client, response=response)


def sync(
    *,
    client: AuthenticatedClient,
    active: Union[Unset, bool] = UNSET,
    cursor: Union[Unset, str] = UNSET,
    first_name: Union[Unset, str] = UNSET,
    last_name: Union[Unset, str] = UNSET,
    page_size: Union[Unset, int] = UNSET,
    role_type: Union[Unset, str] = UNSET,
    verified: Union[Unset, bool] = UNSET,
) -> Optional[PaginatedPersonList]:
    """Person API

    Args:
        active (Union[Unset, bool]):
        cursor (Union[Unset, str]):
        first_name (Union[Unset, str]):
        last_name (Union[Unset, str]):
        page_size (Union[Unset, int]):
        role_type (Union[Unset, str]):
        verified (Union[Unset, bool]):

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        PaginatedPersonList
    """

    return sync_detailed(
        client=client,
        active=active,
        cursor=cursor,
        first_name=first_name,
        last_name=last_name,
        page_size=page_size,
        role_type=role_type,
        verified=verified,
    ).parsed


async def asyncio_detailed(
    *,
    client: AuthenticatedClient,
    active: Union[Unset, bool] = UNSET,
    cursor: Union[Unset, str] = UNSET,
    first_name: Union[Unset, str] = UNSET,
    last_name: Union[Unset, str] = UNSET,
    page_size: Union[Unset, int] = UNSET,
    role_type: Union[Unset, str] = UNSET,
    verified: Union[Unset, bool] = UNSET,
) -> Response[PaginatedPersonList]:
    """Person API

    Args:
        active (Union[Unset, bool]):
        cursor (Union[Unset, str]):
        first_name (Union[Unset, str]):
        last_name (Union[Unset, str]):
        page_size (Union[Unset, int]):
        role_type (Union[Unset, str]):
        verified (Union[Unset, bool]):

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        Response[PaginatedPersonList]
    """

    kwargs = _get_kwargs(
        active=active,
        cursor=cursor,
        first_name=first_name,
        last_name=last_name,
        page_size=page_size,
        role_type=role_type,
        verified=verified,
    )

    response = await client.get_async_httpx_client().request(**kwargs)

    return _build_response(client=client, response=response)


async def asyncio(
    *,
    client: AuthenticatedClient,
    active: Union[Unset, bool] = UNSET,
    cursor: Union[Unset, str] = UNSET,
    first_name: Union[Unset, str] = UNSET,
    last_name: Union[Unset, str] = UNSET,
    page_size: Union[Unset, int] = UNSET,
    role_type: Union[Unset, str] = UNSET,
    verified: Union[Unset, bool] = UNSET,
) -> Optional[PaginatedPersonList]:
    """Person API

    Args:
        active (Union[Unset, bool]):
        cursor (Union[Unset, str]):
        first_name (Union[Unset, str]):
        last_name (Union[Unset, str]):
        page_size (Union[Unset, int]):
        role_type (Union[Unset, str]):
        verified (Union[Unset, bool]):

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        PaginatedPersonList
    """

    return (
        await asyncio_detailed(
            client=client,
            active=active,
            cursor=cursor,
            first_name=first_name,
            last_name=last_name,
            page_size=page_size,
            role_type=role_type,
            verified=verified,
        )
    ).parsed
