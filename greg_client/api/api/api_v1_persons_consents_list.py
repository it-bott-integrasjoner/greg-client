from http import HTTPStatus
from typing import Any, Dict, Optional, Union

import httpx

from ... import errors
from ...client import AuthenticatedClient, Client
from ...models.paginated_consent_serializer_brief_list import PaginatedConsentSerializerBriefList
from ...types import UNSET, Response, Unset


def _get_kwargs(
    person_id: str,
    *,
    cursor: Union[Unset, str] = UNSET,
    id: Union[Unset, int] = UNSET,
    page_size: Union[Unset, int] = UNSET,
) -> Dict[str, Any]:
    params: Dict[str, Any] = {}

    params["cursor"] = cursor

    params["id"] = id

    params["page_size"] = page_size

    params = {k: v for k, v in params.items() if v is not UNSET and v is not None}

    _kwargs: Dict[str, Any] = {
        "method": "get",
        "url": f"/v1/persons/{person_id}/consents",
        "params": params,
    }

    return _kwargs


def _parse_response(
    *, client: Union[AuthenticatedClient, Client], response: httpx.Response
) -> Optional[PaginatedConsentSerializerBriefList]:
    if response.status_code == HTTPStatus.OK:
        response_200 = PaginatedConsentSerializerBriefList.from_dict(response.json())

        return response_200
    if client.raise_on_unexpected_status:
        raise errors.UnexpectedStatus(response.status_code, response.content)
    else:
        return None


def _build_response(
    *, client: Union[AuthenticatedClient, Client], response: httpx.Response
) -> Response[PaginatedConsentSerializerBriefList]:
    return Response(
        status_code=HTTPStatus(response.status_code),
        content=response.content,
        headers=response.headers,
        parsed=_parse_response(client=client, response=response),
    )


def sync_detailed(
    person_id: str,
    *,
    client: AuthenticatedClient,
    cursor: Union[Unset, str] = UNSET,
    id: Union[Unset, int] = UNSET,
    page_size: Union[Unset, int] = UNSET,
) -> Response[PaginatedConsentSerializerBriefList]:
    """Person consent API

    Args:
        person_id (str):
        cursor (Union[Unset, str]):
        id (Union[Unset, int]):
        page_size (Union[Unset, int]):

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        Response[PaginatedConsentSerializerBriefList]
    """

    kwargs = _get_kwargs(
        person_id=person_id,
        cursor=cursor,
        id=id,
        page_size=page_size,
    )

    response = client.get_httpx_client().request(
        **kwargs,
    )

    return _build_response(client=client, response=response)


def sync(
    person_id: str,
    *,
    client: AuthenticatedClient,
    cursor: Union[Unset, str] = UNSET,
    id: Union[Unset, int] = UNSET,
    page_size: Union[Unset, int] = UNSET,
) -> Optional[PaginatedConsentSerializerBriefList]:
    """Person consent API

    Args:
        person_id (str):
        cursor (Union[Unset, str]):
        id (Union[Unset, int]):
        page_size (Union[Unset, int]):

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        PaginatedConsentSerializerBriefList
    """

    return sync_detailed(
        person_id=person_id,
        client=client,
        cursor=cursor,
        id=id,
        page_size=page_size,
    ).parsed


async def asyncio_detailed(
    person_id: str,
    *,
    client: AuthenticatedClient,
    cursor: Union[Unset, str] = UNSET,
    id: Union[Unset, int] = UNSET,
    page_size: Union[Unset, int] = UNSET,
) -> Response[PaginatedConsentSerializerBriefList]:
    """Person consent API

    Args:
        person_id (str):
        cursor (Union[Unset, str]):
        id (Union[Unset, int]):
        page_size (Union[Unset, int]):

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        Response[PaginatedConsentSerializerBriefList]
    """

    kwargs = _get_kwargs(
        person_id=person_id,
        cursor=cursor,
        id=id,
        page_size=page_size,
    )

    response = await client.get_async_httpx_client().request(**kwargs)

    return _build_response(client=client, response=response)


async def asyncio(
    person_id: str,
    *,
    client: AuthenticatedClient,
    cursor: Union[Unset, str] = UNSET,
    id: Union[Unset, int] = UNSET,
    page_size: Union[Unset, int] = UNSET,
) -> Optional[PaginatedConsentSerializerBriefList]:
    """Person consent API

    Args:
        person_id (str):
        cursor (Union[Unset, str]):
        id (Union[Unset, int]):
        page_size (Union[Unset, int]):

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        PaginatedConsentSerializerBriefList
    """

    return (
        await asyncio_detailed(
            person_id=person_id,
            client=client,
            cursor=cursor,
            id=id,
            page_size=page_size,
        )
    ).parsed
