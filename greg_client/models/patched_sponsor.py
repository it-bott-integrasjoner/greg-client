import datetime
import json
from typing import TYPE_CHECKING, Any, Dict, List, Tuple, Type, TypeVar, Union, cast

from attrs import define as _attrs_define
from attrs import field as _attrs_field
from dateutil.parser import isoparse

from ..types import UNSET, Unset

if TYPE_CHECKING:
    from ..models.organizational_unit import OrganizationalUnit


T = TypeVar("T", bound="PatchedSponsor")


@_attrs_define
class PatchedSponsor:
    """
    Attributes:
        id (Union[Unset, int]):
        feide_id (Union[Unset, str]):
        first_name (Union[Unset, str]):
        last_name (Union[Unset, str]):
        work_email (Union[None, Unset, str]):
        orgunits (Union[Unset, List['OrganizationalUnit']]):
        created (Union[Unset, datetime.datetime]):
        updated (Union[Unset, datetime.datetime]):
    """

    id: Union[Unset, int] = UNSET
    feide_id: Union[Unset, str] = UNSET
    first_name: Union[Unset, str] = UNSET
    last_name: Union[Unset, str] = UNSET
    work_email: Union[None, Unset, str] = UNSET
    orgunits: Union[Unset, List["OrganizationalUnit"]] = UNSET
    created: Union[Unset, datetime.datetime] = UNSET
    updated: Union[Unset, datetime.datetime] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        id = self.id

        feide_id = self.feide_id

        first_name = self.first_name

        last_name = self.last_name

        work_email: Union[None, Unset, str]
        if isinstance(self.work_email, Unset):
            work_email = UNSET
        else:
            work_email = self.work_email

        orgunits: Union[Unset, List[Dict[str, Any]]] = UNSET
        if not isinstance(self.orgunits, Unset):
            orgunits = []
            for orgunits_item_data in self.orgunits:
                orgunits_item = orgunits_item_data.to_dict()
                orgunits.append(orgunits_item)

        created: Union[Unset, str] = UNSET
        if not isinstance(self.created, Unset):
            created = self.created.isoformat()

        updated: Union[Unset, str] = UNSET
        if not isinstance(self.updated, Unset):
            updated = self.updated.isoformat()

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update({})
        if id is not UNSET:
            field_dict["id"] = id
        if feide_id is not UNSET:
            field_dict["feide_id"] = feide_id
        if first_name is not UNSET:
            field_dict["first_name"] = first_name
        if last_name is not UNSET:
            field_dict["last_name"] = last_name
        if work_email is not UNSET:
            field_dict["work_email"] = work_email
        if orgunits is not UNSET:
            field_dict["orgunits"] = orgunits
        if created is not UNSET:
            field_dict["created"] = created
        if updated is not UNSET:
            field_dict["updated"] = updated

        return field_dict

    def to_multipart(self) -> Dict[str, Any]:
        id = self.id if isinstance(self.id, Unset) else (None, str(self.id).encode(), "text/plain")

        feide_id = (
            self.feide_id if isinstance(self.feide_id, Unset) else (None, str(self.feide_id).encode(), "text/plain")
        )

        first_name = (
            self.first_name
            if isinstance(self.first_name, Unset)
            else (None, str(self.first_name).encode(), "text/plain")
        )

        last_name = (
            self.last_name if isinstance(self.last_name, Unset) else (None, str(self.last_name).encode(), "text/plain")
        )

        work_email: Union[None, Unset, str]
        if isinstance(self.work_email, Unset):
            work_email = UNSET
        else:
            work_email = self.work_email

        orgunits: Union[Unset, Tuple[None, bytes, str]] = UNSET
        if not isinstance(self.orgunits, Unset):
            _temp_orgunits = []
            for orgunits_item_data in self.orgunits:
                orgunits_item = orgunits_item_data.to_dict()
                _temp_orgunits.append(orgunits_item)
            orgunits = (None, json.dumps(_temp_orgunits).encode(), "application/json")

        created: Union[Unset, bytes] = UNSET
        if not isinstance(self.created, Unset):
            created = self.created.isoformat().encode()

        updated: Union[Unset, bytes] = UNSET
        if not isinstance(self.updated, Unset):
            updated = self.updated.isoformat().encode()

        field_dict: Dict[str, Any] = {}
        field_dict.update(
            {key: (None, str(value).encode(), "text/plain") for key, value in self.additional_properties.items()}
        )
        field_dict.update({})
        if id is not UNSET:
            field_dict["id"] = id
        if feide_id is not UNSET:
            field_dict["feide_id"] = feide_id
        if first_name is not UNSET:
            field_dict["first_name"] = first_name
        if last_name is not UNSET:
            field_dict["last_name"] = last_name
        if work_email is not UNSET:
            field_dict["work_email"] = work_email
        if orgunits is not UNSET:
            field_dict["orgunits"] = orgunits
        if created is not UNSET:
            field_dict["created"] = created
        if updated is not UNSET:
            field_dict["updated"] = updated

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.organizational_unit import OrganizationalUnit

        d = src_dict.copy()
        id = d.pop("id", UNSET)

        feide_id = d.pop("feide_id", UNSET)

        first_name = d.pop("first_name", UNSET)

        last_name = d.pop("last_name", UNSET)

        def _parse_work_email(data: object) -> Union[None, Unset, str]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, str], data)

        work_email = _parse_work_email(d.pop("work_email", UNSET))

        orgunits = []
        _orgunits = d.pop("orgunits", UNSET)
        for orgunits_item_data in _orgunits or []:
            orgunits_item = OrganizationalUnit.from_dict(orgunits_item_data)

            orgunits.append(orgunits_item)

        _created = d.pop("created", UNSET)
        created: Union[Unset, datetime.datetime]
        if isinstance(_created, Unset):
            created = UNSET
        else:
            created = isoparse(_created)

        _updated = d.pop("updated", UNSET)
        updated: Union[Unset, datetime.datetime]
        if isinstance(_updated, Unset):
            updated = UNSET
        else:
            updated = isoparse(_updated)

        patched_sponsor = cls(
            id=id,
            feide_id=feide_id,
            first_name=first_name,
            last_name=last_name,
            work_email=work_email,
            orgunits=orgunits,
            created=created,
            updated=updated,
        )

        patched_sponsor.additional_properties = d
        return patched_sponsor

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
