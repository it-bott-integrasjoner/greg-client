from http import HTTPStatus
from typing import Any, Dict, Optional, Union

import httpx

from ... import errors
from ...client import AuthenticatedClient, Client
from ...models.person_consent import PersonConsent
from ...types import Response


def _get_kwargs(
    person_id: str,
    *,
    body: Union[
        PersonConsent,
        PersonConsent,
        PersonConsent,
    ],
) -> Dict[str, Any]:
    headers: Dict[str, Any] = {}

    _kwargs: Dict[str, Any] = {
        "method": "post",
        "url": f"/v1/persons/{person_id}/consents",
    }

    if isinstance(body, PersonConsent):
        _json_body = body.to_dict()

        _kwargs["json"] = _json_body
        headers["Content-Type"] = "application/json"
    if isinstance(body, PersonConsent):
        _data_body = body.to_dict()

        _kwargs["data"] = _data_body
        headers["Content-Type"] = "application/x-www-form-urlencoded"
    if isinstance(body, PersonConsent):
        _files_body = body.to_multipart()

        _kwargs["files"] = _files_body
        headers["Content-Type"] = "multipart/form-data"

    _kwargs["headers"] = headers
    return _kwargs


def _parse_response(*, client: Union[AuthenticatedClient, Client], response: httpx.Response) -> Optional[PersonConsent]:
    if response.status_code == HTTPStatus.CREATED:
        response_201 = PersonConsent.from_dict(response.json())

        return response_201
    if client.raise_on_unexpected_status:
        raise errors.UnexpectedStatus(response.status_code, response.content)
    else:
        return None


def _build_response(*, client: Union[AuthenticatedClient, Client], response: httpx.Response) -> Response[PersonConsent]:
    return Response(
        status_code=HTTPStatus(response.status_code),
        content=response.content,
        headers=response.headers,
        parsed=_parse_response(client=client, response=response),
    )


def sync_detailed(
    person_id: str,
    *,
    client: AuthenticatedClient,
    body: Union[
        PersonConsent,
        PersonConsent,
        PersonConsent,
    ],
) -> Response[PersonConsent]:
    """Person consent API

    Args:
        person_id (str):
        body (PersonConsent):
        body (PersonConsent):
        body (PersonConsent):

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        Response[PersonConsent]
    """

    kwargs = _get_kwargs(
        person_id=person_id,
        body=body,
    )

    response = client.get_httpx_client().request(
        **kwargs,
    )

    return _build_response(client=client, response=response)


def sync(
    person_id: str,
    *,
    client: AuthenticatedClient,
    body: Union[
        PersonConsent,
        PersonConsent,
        PersonConsent,
    ],
) -> Optional[PersonConsent]:
    """Person consent API

    Args:
        person_id (str):
        body (PersonConsent):
        body (PersonConsent):
        body (PersonConsent):

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        PersonConsent
    """

    return sync_detailed(
        person_id=person_id,
        client=client,
        body=body,
    ).parsed


async def asyncio_detailed(
    person_id: str,
    *,
    client: AuthenticatedClient,
    body: Union[
        PersonConsent,
        PersonConsent,
        PersonConsent,
    ],
) -> Response[PersonConsent]:
    """Person consent API

    Args:
        person_id (str):
        body (PersonConsent):
        body (PersonConsent):
        body (PersonConsent):

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        Response[PersonConsent]
    """

    kwargs = _get_kwargs(
        person_id=person_id,
        body=body,
    )

    response = await client.get_async_httpx_client().request(**kwargs)

    return _build_response(client=client, response=response)


async def asyncio(
    person_id: str,
    *,
    client: AuthenticatedClient,
    body: Union[
        PersonConsent,
        PersonConsent,
        PersonConsent,
    ],
) -> Optional[PersonConsent]:
    """Person consent API

    Args:
        person_id (str):
        body (PersonConsent):
        body (PersonConsent):
        body (PersonConsent):

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        PersonConsent
    """

    return (
        await asyncio_detailed(
            person_id=person_id,
            client=client,
            body=body,
        )
    ).parsed
