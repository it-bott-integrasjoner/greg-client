import datetime
import json
from typing import TYPE_CHECKING, Any, Dict, List, Tuple, Type, TypeVar, Union, cast

from attrs import define as _attrs_define
from attrs import field as _attrs_field
from dateutil.parser import isoparse

from ..types import UNSET, Unset

if TYPE_CHECKING:
    from ..models.ou_identifier import OuIdentifier


T = TypeVar("T", bound="PatchedOrganizationalUnit")


@_attrs_define
class PatchedOrganizationalUnit:
    """
    Attributes:
        id (Union[Unset, int]):
        created (Union[Unset, datetime.datetime]):
        updated (Union[Unset, datetime.datetime]):
        name_nb (Union[Unset, str]):
        name_en (Union[Unset, str]):
        active (Union[Unset, bool]):
        deleted (Union[Unset, bool]):
        parent (Union[None, Unset, int]):
        identifiers (Union[Unset, List['OuIdentifier']]):
    """

    id: Union[Unset, int] = UNSET
    created: Union[Unset, datetime.datetime] = UNSET
    updated: Union[Unset, datetime.datetime] = UNSET
    name_nb: Union[Unset, str] = UNSET
    name_en: Union[Unset, str] = UNSET
    active: Union[Unset, bool] = UNSET
    deleted: Union[Unset, bool] = UNSET
    parent: Union[None, Unset, int] = UNSET
    identifiers: Union[Unset, List["OuIdentifier"]] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        id = self.id

        created: Union[Unset, str] = UNSET
        if not isinstance(self.created, Unset):
            created = self.created.isoformat()

        updated: Union[Unset, str] = UNSET
        if not isinstance(self.updated, Unset):
            updated = self.updated.isoformat()

        name_nb = self.name_nb

        name_en = self.name_en

        active = self.active

        deleted = self.deleted

        parent: Union[None, Unset, int]
        if isinstance(self.parent, Unset):
            parent = UNSET
        else:
            parent = self.parent

        identifiers: Union[Unset, List[Dict[str, Any]]] = UNSET
        if not isinstance(self.identifiers, Unset):
            identifiers = []
            for identifiers_item_data in self.identifiers:
                identifiers_item = identifiers_item_data.to_dict()
                identifiers.append(identifiers_item)

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update({})
        if id is not UNSET:
            field_dict["id"] = id
        if created is not UNSET:
            field_dict["created"] = created
        if updated is not UNSET:
            field_dict["updated"] = updated
        if name_nb is not UNSET:
            field_dict["name_nb"] = name_nb
        if name_en is not UNSET:
            field_dict["name_en"] = name_en
        if active is not UNSET:
            field_dict["active"] = active
        if deleted is not UNSET:
            field_dict["deleted"] = deleted
        if parent is not UNSET:
            field_dict["parent"] = parent
        if identifiers is not UNSET:
            field_dict["identifiers"] = identifiers

        return field_dict

    def to_multipart(self) -> Dict[str, Any]:
        id = self.id if isinstance(self.id, Unset) else (None, str(self.id).encode(), "text/plain")

        created: Union[Unset, bytes] = UNSET
        if not isinstance(self.created, Unset):
            created = self.created.isoformat().encode()

        updated: Union[Unset, bytes] = UNSET
        if not isinstance(self.updated, Unset):
            updated = self.updated.isoformat().encode()

        name_nb = self.name_nb if isinstance(self.name_nb, Unset) else (None, str(self.name_nb).encode(), "text/plain")

        name_en = self.name_en if isinstance(self.name_en, Unset) else (None, str(self.name_en).encode(), "text/plain")

        active = self.active if isinstance(self.active, Unset) else (None, str(self.active).encode(), "text/plain")

        deleted = self.deleted if isinstance(self.deleted, Unset) else (None, str(self.deleted).encode(), "text/plain")

        parent: Union[None, Unset, int]
        if isinstance(self.parent, Unset):
            parent = UNSET
        else:
            parent = self.parent

        identifiers: Union[Unset, Tuple[None, bytes, str]] = UNSET
        if not isinstance(self.identifiers, Unset):
            _temp_identifiers = []
            for identifiers_item_data in self.identifiers:
                identifiers_item = identifiers_item_data.to_dict()
                _temp_identifiers.append(identifiers_item)
            identifiers = (None, json.dumps(_temp_identifiers).encode(), "application/json")

        field_dict: Dict[str, Any] = {}
        field_dict.update(
            {key: (None, str(value).encode(), "text/plain") for key, value in self.additional_properties.items()}
        )
        field_dict.update({})
        if id is not UNSET:
            field_dict["id"] = id
        if created is not UNSET:
            field_dict["created"] = created
        if updated is not UNSET:
            field_dict["updated"] = updated
        if name_nb is not UNSET:
            field_dict["name_nb"] = name_nb
        if name_en is not UNSET:
            field_dict["name_en"] = name_en
        if active is not UNSET:
            field_dict["active"] = active
        if deleted is not UNSET:
            field_dict["deleted"] = deleted
        if parent is not UNSET:
            field_dict["parent"] = parent
        if identifiers is not UNSET:
            field_dict["identifiers"] = identifiers

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.ou_identifier import OuIdentifier

        d = src_dict.copy()
        id = d.pop("id", UNSET)

        _created = d.pop("created", UNSET)
        created: Union[Unset, datetime.datetime]
        if isinstance(_created, Unset):
            created = UNSET
        else:
            created = isoparse(_created)

        _updated = d.pop("updated", UNSET)
        updated: Union[Unset, datetime.datetime]
        if isinstance(_updated, Unset):
            updated = UNSET
        else:
            updated = isoparse(_updated)

        name_nb = d.pop("name_nb", UNSET)

        name_en = d.pop("name_en", UNSET)

        active = d.pop("active", UNSET)

        deleted = d.pop("deleted", UNSET)

        def _parse_parent(data: object) -> Union[None, Unset, int]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, int], data)

        parent = _parse_parent(d.pop("parent", UNSET))

        identifiers = []
        _identifiers = d.pop("identifiers", UNSET)
        for identifiers_item_data in _identifiers or []:
            identifiers_item = OuIdentifier.from_dict(identifiers_item_data)

            identifiers.append(identifiers_item)

        patched_organizational_unit = cls(
            id=id,
            created=created,
            updated=updated,
            name_nb=name_nb,
            name_en=name_en,
            active=active,
            deleted=deleted,
            parent=parent,
            identifiers=identifiers,
        )

        patched_organizational_unit.additional_properties = d
        return patched_organizational_unit

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
