from http import HTTPStatus
from typing import Any, Dict, Optional, Union

import httpx

from ... import errors
from ...client import AuthenticatedClient, Client
from ...models.paginated_sponsor_org_units_list import PaginatedSponsorOrgUnitsList
from ...types import UNSET, Response, Unset


def _get_kwargs(
    sponsor_id: str,
    *,
    cursor: Union[Unset, str] = UNSET,
    page_size: Union[Unset, int] = UNSET,
) -> Dict[str, Any]:
    params: Dict[str, Any] = {}

    params["cursor"] = cursor

    params["page_size"] = page_size

    params = {k: v for k, v in params.items() if v is not UNSET and v is not None}

    _kwargs: Dict[str, Any] = {
        "method": "get",
        "url": f"/v1/sponsors/{sponsor_id}/orgunits",
        "params": params,
    }

    return _kwargs


def _parse_response(
    *, client: Union[AuthenticatedClient, Client], response: httpx.Response
) -> Optional[PaginatedSponsorOrgUnitsList]:
    if response.status_code == HTTPStatus.OK:
        response_200 = PaginatedSponsorOrgUnitsList.from_dict(response.json())

        return response_200
    if client.raise_on_unexpected_status:
        raise errors.UnexpectedStatus(response.status_code, response.content)
    else:
        return None


def _build_response(
    *, client: Union[AuthenticatedClient, Client], response: httpx.Response
) -> Response[PaginatedSponsorOrgUnitsList]:
    return Response(
        status_code=HTTPStatus(response.status_code),
        content=response.content,
        headers=response.headers,
        parsed=_parse_response(client=client, response=response),
    )


def sync_detailed(
    sponsor_id: str,
    *,
    client: AuthenticatedClient,
    cursor: Union[Unset, str] = UNSET,
    page_size: Union[Unset, int] = UNSET,
) -> Response[PaginatedSponsorOrgUnitsList]:
    """Lists all SponsorOrganizationalUnit objects connected to the sponsor

    Args:
        sponsor_id (str):
        cursor (Union[Unset, str]):
        page_size (Union[Unset, int]):

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        Response[PaginatedSponsorOrgUnitsList]
    """

    kwargs = _get_kwargs(
        sponsor_id=sponsor_id,
        cursor=cursor,
        page_size=page_size,
    )

    response = client.get_httpx_client().request(
        **kwargs,
    )

    return _build_response(client=client, response=response)


def sync(
    sponsor_id: str,
    *,
    client: AuthenticatedClient,
    cursor: Union[Unset, str] = UNSET,
    page_size: Union[Unset, int] = UNSET,
) -> Optional[PaginatedSponsorOrgUnitsList]:
    """Lists all SponsorOrganizationalUnit objects connected to the sponsor

    Args:
        sponsor_id (str):
        cursor (Union[Unset, str]):
        page_size (Union[Unset, int]):

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        PaginatedSponsorOrgUnitsList
    """

    return sync_detailed(
        sponsor_id=sponsor_id,
        client=client,
        cursor=cursor,
        page_size=page_size,
    ).parsed


async def asyncio_detailed(
    sponsor_id: str,
    *,
    client: AuthenticatedClient,
    cursor: Union[Unset, str] = UNSET,
    page_size: Union[Unset, int] = UNSET,
) -> Response[PaginatedSponsorOrgUnitsList]:
    """Lists all SponsorOrganizationalUnit objects connected to the sponsor

    Args:
        sponsor_id (str):
        cursor (Union[Unset, str]):
        page_size (Union[Unset, int]):

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        Response[PaginatedSponsorOrgUnitsList]
    """

    kwargs = _get_kwargs(
        sponsor_id=sponsor_id,
        cursor=cursor,
        page_size=page_size,
    )

    response = await client.get_async_httpx_client().request(**kwargs)

    return _build_response(client=client, response=response)


async def asyncio(
    sponsor_id: str,
    *,
    client: AuthenticatedClient,
    cursor: Union[Unset, str] = UNSET,
    page_size: Union[Unset, int] = UNSET,
) -> Optional[PaginatedSponsorOrgUnitsList]:
    """Lists all SponsorOrganizationalUnit objects connected to the sponsor

    Args:
        sponsor_id (str):
        cursor (Union[Unset, str]):
        page_size (Union[Unset, int]):

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        PaginatedSponsorOrgUnitsList
    """

    return (
        await asyncio_detailed(
            sponsor_id=sponsor_id,
            client=client,
            cursor=cursor,
            page_size=page_size,
        )
    ).parsed
