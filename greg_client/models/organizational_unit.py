import datetime
import json
from typing import TYPE_CHECKING, Any, Dict, List, Type, TypeVar, Union, cast

from attrs import define as _attrs_define
from attrs import field as _attrs_field
from dateutil.parser import isoparse

from ..types import UNSET, Unset

if TYPE_CHECKING:
    from ..models.ou_identifier import OuIdentifier


T = TypeVar("T", bound="OrganizationalUnit")


@_attrs_define
class OrganizationalUnit:
    """
    Attributes:
        id (int):
        created (datetime.datetime):
        updated (datetime.datetime):
        name_nb (str):
        name_en (str):
        identifiers (List['OuIdentifier']):
        active (Union[Unset, bool]):
        deleted (Union[Unset, bool]):
        parent (Union[None, Unset, int]):
    """

    id: int
    created: datetime.datetime
    updated: datetime.datetime
    name_nb: str
    name_en: str
    identifiers: List["OuIdentifier"]
    active: Union[Unset, bool] = UNSET
    deleted: Union[Unset, bool] = UNSET
    parent: Union[None, Unset, int] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        id = self.id

        created = self.created.isoformat()

        updated = self.updated.isoformat()

        name_nb = self.name_nb

        name_en = self.name_en

        identifiers = []
        for identifiers_item_data in self.identifiers:
            identifiers_item = identifiers_item_data.to_dict()
            identifiers.append(identifiers_item)

        active = self.active

        deleted = self.deleted

        parent: Union[None, Unset, int]
        if isinstance(self.parent, Unset):
            parent = UNSET
        else:
            parent = self.parent

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "id": id,
                "created": created,
                "updated": updated,
                "name_nb": name_nb,
                "name_en": name_en,
                "identifiers": identifiers,
            }
        )
        if active is not UNSET:
            field_dict["active"] = active
        if deleted is not UNSET:
            field_dict["deleted"] = deleted
        if parent is not UNSET:
            field_dict["parent"] = parent

        return field_dict

    def to_multipart(self) -> Dict[str, Any]:
        id = self.id if isinstance(self.id, Unset) else (None, str(self.id).encode(), "text/plain")

        created = self.created.isoformat().encode()

        updated = self.updated.isoformat().encode()

        name_nb = self.name_nb if isinstance(self.name_nb, Unset) else (None, str(self.name_nb).encode(), "text/plain")

        name_en = self.name_en if isinstance(self.name_en, Unset) else (None, str(self.name_en).encode(), "text/plain")

        _temp_identifiers = []
        for identifiers_item_data in self.identifiers:
            identifiers_item = identifiers_item_data.to_dict()
            _temp_identifiers.append(identifiers_item)
        identifiers = (None, json.dumps(_temp_identifiers).encode(), "application/json")

        active = self.active if isinstance(self.active, Unset) else (None, str(self.active).encode(), "text/plain")

        deleted = self.deleted if isinstance(self.deleted, Unset) else (None, str(self.deleted).encode(), "text/plain")

        parent: Union[None, Unset, int]
        if isinstance(self.parent, Unset):
            parent = UNSET
        else:
            parent = self.parent

        field_dict: Dict[str, Any] = {}
        field_dict.update(
            {key: (None, str(value).encode(), "text/plain") for key, value in self.additional_properties.items()}
        )
        field_dict.update(
            {
                "id": id,
                "created": created,
                "updated": updated,
                "name_nb": name_nb,
                "name_en": name_en,
                "identifiers": identifiers,
            }
        )
        if active is not UNSET:
            field_dict["active"] = active
        if deleted is not UNSET:
            field_dict["deleted"] = deleted
        if parent is not UNSET:
            field_dict["parent"] = parent

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.ou_identifier import OuIdentifier

        d = src_dict.copy()
        id = d.pop("id")

        created = isoparse(d.pop("created"))

        updated = isoparse(d.pop("updated"))

        name_nb = d.pop("name_nb")

        name_en = d.pop("name_en")

        identifiers = []
        _identifiers = d.pop("identifiers")
        for identifiers_item_data in _identifiers:
            identifiers_item = OuIdentifier.from_dict(identifiers_item_data)

            identifiers.append(identifiers_item)

        active = d.pop("active", UNSET)

        deleted = d.pop("deleted", UNSET)

        def _parse_parent(data: object) -> Union[None, Unset, int]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, int], data)

        parent = _parse_parent(d.pop("parent", UNSET))

        organizational_unit = cls(
            id=id,
            created=created,
            updated=updated,
            name_nb=name_nb,
            name_en=name_en,
            identifiers=identifiers,
            active=active,
            deleted=deleted,
            parent=parent,
        )

        organizational_unit.additional_properties = d
        return organizational_unit

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
