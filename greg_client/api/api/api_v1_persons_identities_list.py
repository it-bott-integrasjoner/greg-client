from http import HTTPStatus
from typing import Any, Dict, Optional, Union

import httpx

from ... import errors
from ...client import AuthenticatedClient, Client
from ...models.api_v1_persons_identities_list_type import ApiV1PersonsIdentitiesListType
from ...models.paginated_identity_list import PaginatedIdentityList
from ...types import UNSET, Response, Unset


def _get_kwargs(
    person_id: str,
    *,
    cursor: Union[Unset, str] = UNSET,
    page_size: Union[Unset, int] = UNSET,
    type: Union[Unset, ApiV1PersonsIdentitiesListType] = UNSET,
    verified_by_id: Union[None, Unset, int] = UNSET,
) -> Dict[str, Any]:
    params: Dict[str, Any] = {}

    params["cursor"] = cursor

    params["page_size"] = page_size

    json_type: Union[Unset, str] = UNSET
    if not isinstance(type, Unset):
        json_type = type.value

    params["type"] = json_type

    json_verified_by_id: Union[None, Unset, int]
    if isinstance(verified_by_id, Unset):
        json_verified_by_id = UNSET
    else:
        json_verified_by_id = verified_by_id
    params["verified_by_id"] = json_verified_by_id

    params = {k: v for k, v in params.items() if v is not UNSET and v is not None}

    _kwargs: Dict[str, Any] = {
        "method": "get",
        "url": f"/v1/persons/{person_id}/identities",
        "params": params,
    }

    return _kwargs


def _parse_response(
    *, client: Union[AuthenticatedClient, Client], response: httpx.Response
) -> Optional[PaginatedIdentityList]:
    if response.status_code == HTTPStatus.OK:
        response_200 = PaginatedIdentityList.from_dict(response.json())

        return response_200
    if client.raise_on_unexpected_status:
        raise errors.UnexpectedStatus(response.status_code, response.content)
    else:
        return None


def _build_response(
    *, client: Union[AuthenticatedClient, Client], response: httpx.Response
) -> Response[PaginatedIdentityList]:
    return Response(
        status_code=HTTPStatus(response.status_code),
        content=response.content,
        headers=response.headers,
        parsed=_parse_response(client=client, response=response),
    )


def sync_detailed(
    person_id: str,
    *,
    client: AuthenticatedClient,
    cursor: Union[Unset, str] = UNSET,
    page_size: Union[Unset, int] = UNSET,
    type: Union[Unset, ApiV1PersonsIdentitiesListType] = UNSET,
    verified_by_id: Union[None, Unset, int] = UNSET,
) -> Response[PaginatedIdentityList]:
    """Person identity API

    Args:
        person_id (str):
        cursor (Union[Unset, str]):
        page_size (Union[Unset, int]):
        type (Union[Unset, ApiV1PersonsIdentitiesListType]):
        verified_by_id (Union[None, Unset, int]):

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        Response[PaginatedIdentityList]
    """

    kwargs = _get_kwargs(
        person_id=person_id,
        cursor=cursor,
        page_size=page_size,
        type=type,
        verified_by_id=verified_by_id,
    )

    response = client.get_httpx_client().request(
        **kwargs,
    )

    return _build_response(client=client, response=response)


def sync(
    person_id: str,
    *,
    client: AuthenticatedClient,
    cursor: Union[Unset, str] = UNSET,
    page_size: Union[Unset, int] = UNSET,
    type: Union[Unset, ApiV1PersonsIdentitiesListType] = UNSET,
    verified_by_id: Union[None, Unset, int] = UNSET,
) -> Optional[PaginatedIdentityList]:
    """Person identity API

    Args:
        person_id (str):
        cursor (Union[Unset, str]):
        page_size (Union[Unset, int]):
        type (Union[Unset, ApiV1PersonsIdentitiesListType]):
        verified_by_id (Union[None, Unset, int]):

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        PaginatedIdentityList
    """

    return sync_detailed(
        person_id=person_id,
        client=client,
        cursor=cursor,
        page_size=page_size,
        type=type,
        verified_by_id=verified_by_id,
    ).parsed


async def asyncio_detailed(
    person_id: str,
    *,
    client: AuthenticatedClient,
    cursor: Union[Unset, str] = UNSET,
    page_size: Union[Unset, int] = UNSET,
    type: Union[Unset, ApiV1PersonsIdentitiesListType] = UNSET,
    verified_by_id: Union[None, Unset, int] = UNSET,
) -> Response[PaginatedIdentityList]:
    """Person identity API

    Args:
        person_id (str):
        cursor (Union[Unset, str]):
        page_size (Union[Unset, int]):
        type (Union[Unset, ApiV1PersonsIdentitiesListType]):
        verified_by_id (Union[None, Unset, int]):

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        Response[PaginatedIdentityList]
    """

    kwargs = _get_kwargs(
        person_id=person_id,
        cursor=cursor,
        page_size=page_size,
        type=type,
        verified_by_id=verified_by_id,
    )

    response = await client.get_async_httpx_client().request(**kwargs)

    return _build_response(client=client, response=response)


async def asyncio(
    person_id: str,
    *,
    client: AuthenticatedClient,
    cursor: Union[Unset, str] = UNSET,
    page_size: Union[Unset, int] = UNSET,
    type: Union[Unset, ApiV1PersonsIdentitiesListType] = UNSET,
    verified_by_id: Union[None, Unset, int] = UNSET,
) -> Optional[PaginatedIdentityList]:
    """Person identity API

    Args:
        person_id (str):
        cursor (Union[Unset, str]):
        page_size (Union[Unset, int]):
        type (Union[Unset, ApiV1PersonsIdentitiesListType]):
        verified_by_id (Union[None, Unset, int]):

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        PaginatedIdentityList
    """

    return (
        await asyncio_detailed(
            person_id=person_id,
            client=client,
            cursor=cursor,
            page_size=page_size,
            type=type,
            verified_by_id=verified_by_id,
        )
    ).parsed
