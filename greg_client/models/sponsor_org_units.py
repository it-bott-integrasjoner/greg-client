import datetime
from typing import Any, Dict, List, Type, TypeVar, Union

from attrs import define as _attrs_define
from attrs import field as _attrs_field
from dateutil.parser import isoparse

from ..types import UNSET, Unset

T = TypeVar("T", bound="SponsorOrgUnits")


@_attrs_define
class SponsorOrgUnits:
    """
    Attributes:
        id (int):
        sponsor (int):
        organizational_unit (int):
        hierarchical_access (bool):
        created (datetime.datetime):
        updated (datetime.datetime):
        automatic (Union[Unset, bool]):
        source (Union[Unset, str]):
    """

    id: int
    sponsor: int
    organizational_unit: int
    hierarchical_access: bool
    created: datetime.datetime
    updated: datetime.datetime
    automatic: Union[Unset, bool] = UNSET
    source: Union[Unset, str] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        id = self.id

        sponsor = self.sponsor

        organizational_unit = self.organizational_unit

        hierarchical_access = self.hierarchical_access

        created = self.created.isoformat()

        updated = self.updated.isoformat()

        automatic = self.automatic

        source = self.source

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "id": id,
                "sponsor": sponsor,
                "organizational_unit": organizational_unit,
                "hierarchical_access": hierarchical_access,
                "created": created,
                "updated": updated,
            }
        )
        if automatic is not UNSET:
            field_dict["automatic"] = automatic
        if source is not UNSET:
            field_dict["source"] = source

        return field_dict

    def to_multipart(self) -> Dict[str, Any]:
        id = self.id if isinstance(self.id, Unset) else (None, str(self.id).encode(), "text/plain")

        sponsor = self.sponsor if isinstance(self.sponsor, Unset) else (None, str(self.sponsor).encode(), "text/plain")

        organizational_unit = (
            self.organizational_unit
            if isinstance(self.organizational_unit, Unset)
            else (None, str(self.organizational_unit).encode(), "text/plain")
        )

        hierarchical_access = (
            self.hierarchical_access
            if isinstance(self.hierarchical_access, Unset)
            else (None, str(self.hierarchical_access).encode(), "text/plain")
        )

        created = self.created.isoformat().encode()

        updated = self.updated.isoformat().encode()

        automatic = (
            self.automatic if isinstance(self.automatic, Unset) else (None, str(self.automatic).encode(), "text/plain")
        )

        source = self.source if isinstance(self.source, Unset) else (None, str(self.source).encode(), "text/plain")

        field_dict: Dict[str, Any] = {}
        field_dict.update(
            {key: (None, str(value).encode(), "text/plain") for key, value in self.additional_properties.items()}
        )
        field_dict.update(
            {
                "id": id,
                "sponsor": sponsor,
                "organizational_unit": organizational_unit,
                "hierarchical_access": hierarchical_access,
                "created": created,
                "updated": updated,
            }
        )
        if automatic is not UNSET:
            field_dict["automatic"] = automatic
        if source is not UNSET:
            field_dict["source"] = source

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        d = src_dict.copy()
        id = d.pop("id")

        sponsor = d.pop("sponsor")

        organizational_unit = d.pop("organizational_unit")

        hierarchical_access = d.pop("hierarchical_access")

        created = isoparse(d.pop("created"))

        updated = isoparse(d.pop("updated"))

        automatic = d.pop("automatic", UNSET)

        source = d.pop("source", UNSET)

        sponsor_org_units = cls(
            id=id,
            sponsor=sponsor,
            organizational_unit=organizational_unit,
            hierarchical_access=hierarchical_access,
            created=created,
            updated=updated,
            automatic=automatic,
            source=source,
        )

        sponsor_org_units.additional_properties = d
        return sponsor_org_units

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
