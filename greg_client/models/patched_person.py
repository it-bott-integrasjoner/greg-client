import datetime
import json
from typing import TYPE_CHECKING, Any, Dict, List, Tuple, Type, TypeVar, Union, cast

from attrs import define as _attrs_define
from attrs import field as _attrs_field
from dateutil.parser import isoparse

from ..models.blank_enum import BlankEnum
from ..models.gender_enum import GenderEnum
from ..types import UNSET, Unset

if TYPE_CHECKING:
    from ..models.consent_serializer_brief import ConsentSerializerBrief
    from ..models.identity import Identity
    from ..models.patched_person_meta_type_0 import PatchedPersonMetaType0
    from ..models.role import Role


T = TypeVar("T", bound="PatchedPerson")


@_attrs_define
class PatchedPerson:
    """
    Attributes:
        id (Union[Unset, int]):
        first_name (Union[Unset, str]):
        last_name (Union[Unset, str]):
        gender (Union[BlankEnum, GenderEnum, None, Unset]):
        date_of_birth (Union[None, Unset, datetime.date]):
        registration_completed_date (Union[None, Unset, datetime.date]):
        identities (Union[Unset, List['Identity']]):
        roles (Union[Unset, List['Role']]):
        consents (Union[Unset, List['ConsentSerializerBrief']]):
        meta (Union['PatchedPersonMetaType0', None, Unset]):
        created (Union[Unset, datetime.datetime]):
        updated (Union[Unset, datetime.datetime]):
    """

    id: Union[Unset, int] = UNSET
    first_name: Union[Unset, str] = UNSET
    last_name: Union[Unset, str] = UNSET
    gender: Union[BlankEnum, GenderEnum, None, Unset] = UNSET
    date_of_birth: Union[None, Unset, datetime.date] = UNSET
    registration_completed_date: Union[None, Unset, datetime.date] = UNSET
    identities: Union[Unset, List["Identity"]] = UNSET
    roles: Union[Unset, List["Role"]] = UNSET
    consents: Union[Unset, List["ConsentSerializerBrief"]] = UNSET
    meta: Union["PatchedPersonMetaType0", None, Unset] = UNSET
    created: Union[Unset, datetime.datetime] = UNSET
    updated: Union[Unset, datetime.datetime] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        from ..models.patched_person_meta_type_0 import PatchedPersonMetaType0

        id = self.id

        first_name = self.first_name

        last_name = self.last_name

        gender: Union[None, Unset, str]
        if isinstance(self.gender, Unset):
            gender = UNSET
        elif isinstance(self.gender, GenderEnum):
            gender = self.gender.value
        elif isinstance(self.gender, BlankEnum):
            gender = self.gender.value
        else:
            gender = self.gender

        date_of_birth: Union[None, Unset, str]
        if isinstance(self.date_of_birth, Unset):
            date_of_birth = UNSET
        elif isinstance(self.date_of_birth, datetime.date):
            date_of_birth = self.date_of_birth.isoformat()
        else:
            date_of_birth = self.date_of_birth

        registration_completed_date: Union[None, Unset, str]
        if isinstance(self.registration_completed_date, Unset):
            registration_completed_date = UNSET
        elif isinstance(self.registration_completed_date, datetime.date):
            registration_completed_date = self.registration_completed_date.isoformat()
        else:
            registration_completed_date = self.registration_completed_date

        identities: Union[Unset, List[Dict[str, Any]]] = UNSET
        if not isinstance(self.identities, Unset):
            identities = []
            for identities_item_data in self.identities:
                identities_item = identities_item_data.to_dict()
                identities.append(identities_item)

        roles: Union[Unset, List[Dict[str, Any]]] = UNSET
        if not isinstance(self.roles, Unset):
            roles = []
            for roles_item_data in self.roles:
                roles_item = roles_item_data.to_dict()
                roles.append(roles_item)

        consents: Union[Unset, List[Dict[str, Any]]] = UNSET
        if not isinstance(self.consents, Unset):
            consents = []
            for consents_item_data in self.consents:
                consents_item = consents_item_data.to_dict()
                consents.append(consents_item)

        meta: Union[Dict[str, Any], None, Unset]
        if isinstance(self.meta, Unset):
            meta = UNSET
        elif isinstance(self.meta, PatchedPersonMetaType0):
            meta = self.meta.to_dict()
        else:
            meta = self.meta

        created: Union[Unset, str] = UNSET
        if not isinstance(self.created, Unset):
            created = self.created.isoformat()

        updated: Union[Unset, str] = UNSET
        if not isinstance(self.updated, Unset):
            updated = self.updated.isoformat()

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update({})
        if id is not UNSET:
            field_dict["id"] = id
        if first_name is not UNSET:
            field_dict["first_name"] = first_name
        if last_name is not UNSET:
            field_dict["last_name"] = last_name
        if gender is not UNSET:
            field_dict["gender"] = gender
        if date_of_birth is not UNSET:
            field_dict["date_of_birth"] = date_of_birth
        if registration_completed_date is not UNSET:
            field_dict["registration_completed_date"] = registration_completed_date
        if identities is not UNSET:
            field_dict["identities"] = identities
        if roles is not UNSET:
            field_dict["roles"] = roles
        if consents is not UNSET:
            field_dict["consents"] = consents
        if meta is not UNSET:
            field_dict["meta"] = meta
        if created is not UNSET:
            field_dict["created"] = created
        if updated is not UNSET:
            field_dict["updated"] = updated

        return field_dict

    def to_multipart(self) -> Dict[str, Any]:
        id = self.id if isinstance(self.id, Unset) else (None, str(self.id).encode(), "text/plain")

        first_name = (
            self.first_name
            if isinstance(self.first_name, Unset)
            else (None, str(self.first_name).encode(), "text/plain")
        )

        last_name = (
            self.last_name if isinstance(self.last_name, Unset) else (None, str(self.last_name).encode(), "text/plain")
        )

        gender: Union[BlankEnum, GenderEnum, None, Unset]
        if isinstance(self.gender, Unset):
            gender = UNSET
        elif isinstance(self.gender, GenderEnum):
            gender = (None, str(self.gender.value).encode(), "text/plain")
        elif isinstance(self.gender, BlankEnum):
            gender = (None, str(self.gender.value).encode(), "text/plain")
        else:
            gender = self.gender

        date_of_birth: Union[None, Unset, datetime.date]
        if isinstance(self.date_of_birth, Unset):
            date_of_birth = UNSET
        elif isinstance(self.date_of_birth, datetime.date):
            date_of_birth = self.date_of_birth.isoformat().encode()
        else:
            date_of_birth = self.date_of_birth

        registration_completed_date: Union[None, Unset, datetime.date]
        if isinstance(self.registration_completed_date, Unset):
            registration_completed_date = UNSET
        elif isinstance(self.registration_completed_date, datetime.date):
            registration_completed_date = self.registration_completed_date.isoformat().encode()
        else:
            registration_completed_date = self.registration_completed_date

        identities: Union[Unset, Tuple[None, bytes, str]] = UNSET
        if not isinstance(self.identities, Unset):
            _temp_identities = []
            for identities_item_data in self.identities:
                identities_item = identities_item_data.to_dict()
                _temp_identities.append(identities_item)
            identities = (None, json.dumps(_temp_identities).encode(), "application/json")

        roles: Union[Unset, Tuple[None, bytes, str]] = UNSET
        if not isinstance(self.roles, Unset):
            _temp_roles = []
            for roles_item_data in self.roles:
                roles_item = roles_item_data.to_dict()
                _temp_roles.append(roles_item)
            roles = (None, json.dumps(_temp_roles).encode(), "application/json")

        consents: Union[Unset, Tuple[None, bytes, str]] = UNSET
        if not isinstance(self.consents, Unset):
            _temp_consents = []
            for consents_item_data in self.consents:
                consents_item = consents_item_data.to_dict()
                _temp_consents.append(consents_item)
            consents = (None, json.dumps(_temp_consents).encode(), "application/json")

        meta: Union[None, Tuple[None, bytes, str], Unset]
        if isinstance(self.meta, Unset):
            meta = UNSET
        elif isinstance(self.meta, PatchedPersonMetaType0):
            meta = (None, json.dumps(self.meta.to_dict()).encode(), "application/json")
        else:
            meta = self.meta

        created: Union[Unset, bytes] = UNSET
        if not isinstance(self.created, Unset):
            created = self.created.isoformat().encode()

        updated: Union[Unset, bytes] = UNSET
        if not isinstance(self.updated, Unset):
            updated = self.updated.isoformat().encode()

        field_dict: Dict[str, Any] = {}
        field_dict.update(
            {key: (None, str(value).encode(), "text/plain") for key, value in self.additional_properties.items()}
        )
        field_dict.update({})
        if id is not UNSET:
            field_dict["id"] = id
        if first_name is not UNSET:
            field_dict["first_name"] = first_name
        if last_name is not UNSET:
            field_dict["last_name"] = last_name
        if gender is not UNSET:
            field_dict["gender"] = gender
        if date_of_birth is not UNSET:
            field_dict["date_of_birth"] = date_of_birth
        if registration_completed_date is not UNSET:
            field_dict["registration_completed_date"] = registration_completed_date
        if identities is not UNSET:
            field_dict["identities"] = identities
        if roles is not UNSET:
            field_dict["roles"] = roles
        if consents is not UNSET:
            field_dict["consents"] = consents
        if meta is not UNSET:
            field_dict["meta"] = meta
        if created is not UNSET:
            field_dict["created"] = created
        if updated is not UNSET:
            field_dict["updated"] = updated

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.consent_serializer_brief import ConsentSerializerBrief
        from ..models.identity import Identity
        from ..models.patched_person_meta_type_0 import PatchedPersonMetaType0
        from ..models.role import Role

        d = src_dict.copy()
        id = d.pop("id", UNSET)

        first_name = d.pop("first_name", UNSET)

        last_name = d.pop("last_name", UNSET)

        def _parse_gender(data: object) -> Union[BlankEnum, GenderEnum, None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, str):
                    raise TypeError()
                gender_type_0 = GenderEnum(data)

                return gender_type_0
            except:  # noqa: E722
                pass
            try:
                if not isinstance(data, str):
                    raise TypeError()
                gender_type_1 = BlankEnum(data)

                return gender_type_1
            except:  # noqa: E722
                pass
            return cast(Union[BlankEnum, GenderEnum, None, Unset], data)

        gender = _parse_gender(d.pop("gender", UNSET))

        def _parse_date_of_birth(data: object) -> Union[None, Unset, datetime.date]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, str):
                    raise TypeError()
                date_of_birth_type_0 = isoparse(data).date()

                return date_of_birth_type_0
            except:  # noqa: E722
                pass
            return cast(Union[None, Unset, datetime.date], data)

        date_of_birth = _parse_date_of_birth(d.pop("date_of_birth", UNSET))

        def _parse_registration_completed_date(data: object) -> Union[None, Unset, datetime.date]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, str):
                    raise TypeError()
                registration_completed_date_type_0 = isoparse(data).date()

                return registration_completed_date_type_0
            except:  # noqa: E722
                pass
            return cast(Union[None, Unset, datetime.date], data)

        registration_completed_date = _parse_registration_completed_date(d.pop("registration_completed_date", UNSET))

        identities = []
        _identities = d.pop("identities", UNSET)
        for identities_item_data in _identities or []:
            identities_item = Identity.from_dict(identities_item_data)

            identities.append(identities_item)

        roles = []
        _roles = d.pop("roles", UNSET)
        for roles_item_data in _roles or []:
            roles_item = Role.from_dict(roles_item_data)

            roles.append(roles_item)

        consents = []
        _consents = d.pop("consents", UNSET)
        for consents_item_data in _consents or []:
            consents_item = ConsentSerializerBrief.from_dict(consents_item_data)

            consents.append(consents_item)

        def _parse_meta(data: object) -> Union["PatchedPersonMetaType0", None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                meta_type_0 = PatchedPersonMetaType0.from_dict(data)

                return meta_type_0
            except:  # noqa: E722
                pass
            return cast(Union["PatchedPersonMetaType0", None, Unset], data)

        meta = _parse_meta(d.pop("meta", UNSET))

        _created = d.pop("created", UNSET)
        created: Union[Unset, datetime.datetime]
        if isinstance(_created, Unset):
            created = UNSET
        else:
            created = isoparse(_created)

        _updated = d.pop("updated", UNSET)
        updated: Union[Unset, datetime.datetime]
        if isinstance(_updated, Unset):
            updated = UNSET
        else:
            updated = isoparse(_updated)

        patched_person = cls(
            id=id,
            first_name=first_name,
            last_name=last_name,
            gender=gender,
            date_of_birth=date_of_birth,
            registration_completed_date=registration_completed_date,
            identities=identities,
            roles=roles,
            consents=consents,
            meta=meta,
            created=created,
            updated=updated,
        )

        patched_person.additional_properties = d
        return patched_person

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
