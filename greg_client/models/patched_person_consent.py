import datetime
from typing import Any, Dict, List, Type, TypeVar, Union, cast

from attrs import define as _attrs_define
from attrs import field as _attrs_field
from dateutil.parser import isoparse

from ..types import UNSET, Unset

T = TypeVar("T", bound="PatchedPersonConsent")


@_attrs_define
class PatchedPersonConsent:
    """
    Attributes:
        id (Union[Unset, int]):
        person (Union[Unset, int]):
        type (Union[Unset, int]):
        choice (Union[None, Unset, int]):
        consent_given_at (Union[None, Unset, datetime.date]):
    """

    id: Union[Unset, int] = UNSET
    person: Union[Unset, int] = UNSET
    type: Union[Unset, int] = UNSET
    choice: Union[None, Unset, int] = UNSET
    consent_given_at: Union[None, Unset, datetime.date] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        id = self.id

        person = self.person

        type = self.type

        choice: Union[None, Unset, int]
        if isinstance(self.choice, Unset):
            choice = UNSET
        else:
            choice = self.choice

        consent_given_at: Union[None, Unset, str]
        if isinstance(self.consent_given_at, Unset):
            consent_given_at = UNSET
        elif isinstance(self.consent_given_at, datetime.date):
            consent_given_at = self.consent_given_at.isoformat()
        else:
            consent_given_at = self.consent_given_at

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update({})
        if id is not UNSET:
            field_dict["id"] = id
        if person is not UNSET:
            field_dict["person"] = person
        if type is not UNSET:
            field_dict["type"] = type
        if choice is not UNSET:
            field_dict["choice"] = choice
        if consent_given_at is not UNSET:
            field_dict["consent_given_at"] = consent_given_at

        return field_dict

    def to_multipart(self) -> Dict[str, Any]:
        id = self.id if isinstance(self.id, Unset) else (None, str(self.id).encode(), "text/plain")

        person = self.person if isinstance(self.person, Unset) else (None, str(self.person).encode(), "text/plain")

        type = self.type if isinstance(self.type, Unset) else (None, str(self.type).encode(), "text/plain")

        choice: Union[None, Unset, int]
        if isinstance(self.choice, Unset):
            choice = UNSET
        else:
            choice = self.choice

        consent_given_at: Union[None, Unset, datetime.date]
        if isinstance(self.consent_given_at, Unset):
            consent_given_at = UNSET
        elif isinstance(self.consent_given_at, datetime.date):
            consent_given_at = self.consent_given_at.isoformat().encode()
        else:
            consent_given_at = self.consent_given_at

        field_dict: Dict[str, Any] = {}
        field_dict.update(
            {key: (None, str(value).encode(), "text/plain") for key, value in self.additional_properties.items()}
        )
        field_dict.update({})
        if id is not UNSET:
            field_dict["id"] = id
        if person is not UNSET:
            field_dict["person"] = person
        if type is not UNSET:
            field_dict["type"] = type
        if choice is not UNSET:
            field_dict["choice"] = choice
        if consent_given_at is not UNSET:
            field_dict["consent_given_at"] = consent_given_at

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        d = src_dict.copy()
        id = d.pop("id", UNSET)

        person = d.pop("person", UNSET)

        type = d.pop("type", UNSET)

        def _parse_choice(data: object) -> Union[None, Unset, int]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, int], data)

        choice = _parse_choice(d.pop("choice", UNSET))

        def _parse_consent_given_at(data: object) -> Union[None, Unset, datetime.date]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, str):
                    raise TypeError()
                consent_given_at_type_0 = isoparse(data).date()

                return consent_given_at_type_0
            except:  # noqa: E722
                pass
            return cast(Union[None, Unset, datetime.date], data)

        consent_given_at = _parse_consent_given_at(d.pop("consent_given_at", UNSET))

        patched_person_consent = cls(
            id=id,
            person=person,
            type=type,
            choice=choice,
            consent_given_at=consent_given_at,
        )

        patched_person_consent.additional_properties = d
        return patched_person_consent

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
