import datetime
import json
from typing import TYPE_CHECKING, Any, Dict, List, Type, TypeVar, Union, cast

from attrs import define as _attrs_define
from attrs import field as _attrs_field
from dateutil.parser import isoparse

from ..models.blank_enum import BlankEnum
from ..models.gender_enum import GenderEnum
from ..types import UNSET, Unset

if TYPE_CHECKING:
    from ..models.consent_serializer_brief import ConsentSerializerBrief
    from ..models.identity import Identity
    from ..models.role import Role


T = TypeVar("T", bound="Person")


@_attrs_define
class Person:
    """
    Attributes:
        id (int):
        first_name (str):
        last_name (str):
        identities (List['Identity']):
        roles (List['Role']):
        consents (List['ConsentSerializerBrief']):
        created (datetime.datetime):
        updated (datetime.datetime):
        gender (Union[BlankEnum, GenderEnum, None, Unset]):
        date_of_birth (Union[None, Unset, datetime.date]):
        registration_completed_date (Union[None, Unset, datetime.date]):
        meta (Union[Unset, Any]):
    """

    id: int
    first_name: str
    last_name: str
    identities: List["Identity"]
    roles: List["Role"]
    consents: List["ConsentSerializerBrief"]
    created: datetime.datetime
    updated: datetime.datetime
    gender: Union[BlankEnum, GenderEnum, None, Unset] = UNSET
    date_of_birth: Union[None, Unset, datetime.date] = UNSET
    registration_completed_date: Union[None, Unset, datetime.date] = UNSET
    meta: Union[Unset, Any] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        id = self.id

        first_name = self.first_name

        last_name = self.last_name

        identities = []
        for identities_item_data in self.identities:
            identities_item = identities_item_data.to_dict()
            identities.append(identities_item)

        roles = []
        for roles_item_data in self.roles:
            roles_item = roles_item_data.to_dict()
            roles.append(roles_item)

        consents = []
        for consents_item_data in self.consents:
            consents_item = consents_item_data.to_dict()
            consents.append(consents_item)

        created = self.created.isoformat()

        updated = self.updated.isoformat()

        gender: Union[None, Unset, str]
        if isinstance(self.gender, Unset):
            gender = UNSET
        elif isinstance(self.gender, GenderEnum):
            gender = self.gender.value
        elif isinstance(self.gender, BlankEnum):
            gender = self.gender.value
        else:
            gender = self.gender

        date_of_birth: Union[None, Unset, str]
        if isinstance(self.date_of_birth, Unset):
            date_of_birth = UNSET
        elif isinstance(self.date_of_birth, datetime.date):
            date_of_birth = self.date_of_birth.isoformat()
        else:
            date_of_birth = self.date_of_birth

        registration_completed_date: Union[None, Unset, str]
        if isinstance(self.registration_completed_date, Unset):
            registration_completed_date = UNSET
        elif isinstance(self.registration_completed_date, datetime.date):
            registration_completed_date = self.registration_completed_date.isoformat()
        else:
            registration_completed_date = self.registration_completed_date

        meta = self.meta

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "id": id,
                "first_name": first_name,
                "last_name": last_name,
                "identities": identities,
                "roles": roles,
                "consents": consents,
                "created": created,
                "updated": updated,
            }
        )
        if gender is not UNSET:
            field_dict["gender"] = gender
        if date_of_birth is not UNSET:
            field_dict["date_of_birth"] = date_of_birth
        if registration_completed_date is not UNSET:
            field_dict["registration_completed_date"] = registration_completed_date
        if meta is not UNSET:
            field_dict["meta"] = meta

        return field_dict

    def to_multipart(self) -> Dict[str, Any]:
        id = self.id if isinstance(self.id, Unset) else (None, str(self.id).encode(), "text/plain")

        first_name = (
            self.first_name
            if isinstance(self.first_name, Unset)
            else (None, str(self.first_name).encode(), "text/plain")
        )

        last_name = (
            self.last_name if isinstance(self.last_name, Unset) else (None, str(self.last_name).encode(), "text/plain")
        )

        _temp_identities = []
        for identities_item_data in self.identities:
            identities_item = identities_item_data.to_dict()
            _temp_identities.append(identities_item)
        identities = (None, json.dumps(_temp_identities).encode(), "application/json")

        _temp_roles = []
        for roles_item_data in self.roles:
            roles_item = roles_item_data.to_dict()
            _temp_roles.append(roles_item)
        roles = (None, json.dumps(_temp_roles).encode(), "application/json")

        _temp_consents = []
        for consents_item_data in self.consents:
            consents_item = consents_item_data.to_dict()
            _temp_consents.append(consents_item)
        consents = (None, json.dumps(_temp_consents).encode(), "application/json")

        created = self.created.isoformat().encode()

        updated = self.updated.isoformat().encode()

        gender: Union[BlankEnum, GenderEnum, None, Unset]
        if isinstance(self.gender, Unset):
            gender = UNSET
        elif isinstance(self.gender, GenderEnum):
            gender = (None, str(self.gender.value).encode(), "text/plain")
        elif isinstance(self.gender, BlankEnum):
            gender = (None, str(self.gender.value).encode(), "text/plain")
        else:
            gender = self.gender

        date_of_birth: Union[None, Unset, datetime.date]
        if isinstance(self.date_of_birth, Unset):
            date_of_birth = UNSET
        elif isinstance(self.date_of_birth, datetime.date):
            date_of_birth = self.date_of_birth.isoformat().encode()
        else:
            date_of_birth = self.date_of_birth

        registration_completed_date: Union[None, Unset, datetime.date]
        if isinstance(self.registration_completed_date, Unset):
            registration_completed_date = UNSET
        elif isinstance(self.registration_completed_date, datetime.date):
            registration_completed_date = self.registration_completed_date.isoformat().encode()
        else:
            registration_completed_date = self.registration_completed_date

        meta = self.meta if isinstance(self.meta, Unset) else (None, str(self.meta).encode(), "text/plain")

        field_dict: Dict[str, Any] = {}
        field_dict.update(
            {key: (None, str(value).encode(), "text/plain") for key, value in self.additional_properties.items()}
        )
        field_dict.update(
            {
                "id": id,
                "first_name": first_name,
                "last_name": last_name,
                "identities": identities,
                "roles": roles,
                "consents": consents,
                "created": created,
                "updated": updated,
            }
        )
        if gender is not UNSET:
            field_dict["gender"] = gender
        if date_of_birth is not UNSET:
            field_dict["date_of_birth"] = date_of_birth
        if registration_completed_date is not UNSET:
            field_dict["registration_completed_date"] = registration_completed_date
        if meta is not UNSET:
            field_dict["meta"] = meta

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.consent_serializer_brief import ConsentSerializerBrief
        from ..models.identity import Identity
        from ..models.role import Role

        d = src_dict.copy()
        id = d.pop("id")

        first_name = d.pop("first_name")

        last_name = d.pop("last_name")

        identities = []
        _identities = d.pop("identities")
        for identities_item_data in _identities:
            identities_item = Identity.from_dict(identities_item_data)

            identities.append(identities_item)

        roles = []
        _roles = d.pop("roles")
        for roles_item_data in _roles:
            roles_item = Role.from_dict(roles_item_data)

            roles.append(roles_item)

        consents = []
        _consents = d.pop("consents")
        for consents_item_data in _consents:
            consents_item = ConsentSerializerBrief.from_dict(consents_item_data)

            consents.append(consents_item)

        created = isoparse(d.pop("created"))

        updated = isoparse(d.pop("updated"))

        def _parse_gender(data: object) -> Union[BlankEnum, GenderEnum, None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, str):
                    raise TypeError()
                gender_type_0 = GenderEnum(data)

                return gender_type_0
            except:  # noqa: E722
                pass
            try:
                if not isinstance(data, str):
                    raise TypeError()
                gender_type_1 = BlankEnum(data)

                return gender_type_1
            except:  # noqa: E722
                pass
            return cast(Union[BlankEnum, GenderEnum, None, Unset], data)

        gender = _parse_gender(d.pop("gender", UNSET))

        def _parse_date_of_birth(data: object) -> Union[None, Unset, datetime.date]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, str):
                    raise TypeError()
                date_of_birth_type_0 = isoparse(data).date()

                return date_of_birth_type_0
            except:  # noqa: E722
                pass
            return cast(Union[None, Unset, datetime.date], data)

        date_of_birth = _parse_date_of_birth(d.pop("date_of_birth", UNSET))

        def _parse_registration_completed_date(data: object) -> Union[None, Unset, datetime.date]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, str):
                    raise TypeError()
                registration_completed_date_type_0 = isoparse(data).date()

                return registration_completed_date_type_0
            except:  # noqa: E722
                pass
            return cast(Union[None, Unset, datetime.date], data)

        registration_completed_date = _parse_registration_completed_date(d.pop("registration_completed_date", UNSET))

        meta = d.pop("meta", UNSET)

        person = cls(
            id=id,
            first_name=first_name,
            last_name=last_name,
            identities=identities,
            roles=roles,
            consents=consents,
            created=created,
            updated=updated,
            gender=gender,
            date_of_birth=date_of_birth,
            registration_completed_date=registration_completed_date,
            meta=meta,
        )

        person.additional_properties = d
        return person

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
