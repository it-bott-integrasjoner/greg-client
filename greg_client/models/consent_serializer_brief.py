import datetime
from typing import TYPE_CHECKING, Any, Dict, List, Type, TypeVar, Union, cast

from attrs import define as _attrs_define
from attrs import field as _attrs_field
from dateutil.parser import isoparse

from ..types import UNSET, Unset

if TYPE_CHECKING:
    from ..models.consent_type_serializer_brief import ConsentTypeSerializerBrief


T = TypeVar("T", bound="ConsentSerializerBrief")


@_attrs_define
class ConsentSerializerBrief:
    """
    Attributes:
        id (int):
        type (ConsentTypeSerializerBrief):
        choice (str):
        consent_given_at (Union[None, Unset, datetime.date]):
    """

    id: int
    type: "ConsentTypeSerializerBrief"
    choice: str
    consent_given_at: Union[None, Unset, datetime.date] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        id = self.id

        type = self.type.to_dict()

        choice = self.choice

        consent_given_at: Union[None, Unset, str]
        if isinstance(self.consent_given_at, Unset):
            consent_given_at = UNSET
        elif isinstance(self.consent_given_at, datetime.date):
            consent_given_at = self.consent_given_at.isoformat()
        else:
            consent_given_at = self.consent_given_at

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "id": id,
                "type": type,
                "choice": choice,
            }
        )
        if consent_given_at is not UNSET:
            field_dict["consent_given_at"] = consent_given_at

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.consent_type_serializer_brief import ConsentTypeSerializerBrief

        d = src_dict.copy()
        id = d.pop("id")

        type = ConsentTypeSerializerBrief.from_dict(d.pop("type"))

        choice = d.pop("choice")

        def _parse_consent_given_at(data: object) -> Union[None, Unset, datetime.date]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, str):
                    raise TypeError()
                consent_given_at_type_0 = isoparse(data).date()

                return consent_given_at_type_0
            except:  # noqa: E722
                pass
            return cast(Union[None, Unset, datetime.date], data)

        consent_given_at = _parse_consent_given_at(d.pop("consent_given_at", UNSET))

        consent_serializer_brief = cls(
            id=id,
            type=type,
            choice=choice,
            consent_given_at=consent_given_at,
        )

        consent_serializer_brief.additional_properties = d
        return consent_serializer_brief

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
