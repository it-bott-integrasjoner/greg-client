import datetime
import json
from typing import TYPE_CHECKING, Any, Dict, List, Type, TypeVar, Union, cast

from attrs import define as _attrs_define
from attrs import field as _attrs_field
from dateutil.parser import isoparse

from ..types import UNSET, Unset

if TYPE_CHECKING:
    from ..models.organizational_unit import OrganizationalUnit


T = TypeVar("T", bound="Sponsor")


@_attrs_define
class Sponsor:
    """
    Attributes:
        id (int):
        feide_id (str):
        first_name (str):
        last_name (str):
        orgunits (List['OrganizationalUnit']):
        created (datetime.datetime):
        updated (datetime.datetime):
        work_email (Union[None, Unset, str]):
    """

    id: int
    feide_id: str
    first_name: str
    last_name: str
    orgunits: List["OrganizationalUnit"]
    created: datetime.datetime
    updated: datetime.datetime
    work_email: Union[None, Unset, str] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        id = self.id

        feide_id = self.feide_id

        first_name = self.first_name

        last_name = self.last_name

        orgunits = []
        for orgunits_item_data in self.orgunits:
            orgunits_item = orgunits_item_data.to_dict()
            orgunits.append(orgunits_item)

        created = self.created.isoformat()

        updated = self.updated.isoformat()

        work_email: Union[None, Unset, str]
        if isinstance(self.work_email, Unset):
            work_email = UNSET
        else:
            work_email = self.work_email

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "id": id,
                "feide_id": feide_id,
                "first_name": first_name,
                "last_name": last_name,
                "orgunits": orgunits,
                "created": created,
                "updated": updated,
            }
        )
        if work_email is not UNSET:
            field_dict["work_email"] = work_email

        return field_dict

    def to_multipart(self) -> Dict[str, Any]:
        id = self.id if isinstance(self.id, Unset) else (None, str(self.id).encode(), "text/plain")

        feide_id = (
            self.feide_id if isinstance(self.feide_id, Unset) else (None, str(self.feide_id).encode(), "text/plain")
        )

        first_name = (
            self.first_name
            if isinstance(self.first_name, Unset)
            else (None, str(self.first_name).encode(), "text/plain")
        )

        last_name = (
            self.last_name if isinstance(self.last_name, Unset) else (None, str(self.last_name).encode(), "text/plain")
        )

        _temp_orgunits = []
        for orgunits_item_data in self.orgunits:
            orgunits_item = orgunits_item_data.to_dict()
            _temp_orgunits.append(orgunits_item)
        orgunits = (None, json.dumps(_temp_orgunits).encode(), "application/json")

        created = self.created.isoformat().encode()

        updated = self.updated.isoformat().encode()

        work_email: Union[None, Unset, str]
        if isinstance(self.work_email, Unset):
            work_email = UNSET
        else:
            work_email = self.work_email

        field_dict: Dict[str, Any] = {}
        field_dict.update(
            {key: (None, str(value).encode(), "text/plain") for key, value in self.additional_properties.items()}
        )
        field_dict.update(
            {
                "id": id,
                "feide_id": feide_id,
                "first_name": first_name,
                "last_name": last_name,
                "orgunits": orgunits,
                "created": created,
                "updated": updated,
            }
        )
        if work_email is not UNSET:
            field_dict["work_email"] = work_email

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.organizational_unit import OrganizationalUnit

        d = src_dict.copy()
        id = d.pop("id")

        feide_id = d.pop("feide_id")

        first_name = d.pop("first_name")

        last_name = d.pop("last_name")

        orgunits = []
        _orgunits = d.pop("orgunits")
        for orgunits_item_data in _orgunits:
            orgunits_item = OrganizationalUnit.from_dict(orgunits_item_data)

            orgunits.append(orgunits_item)

        created = isoparse(d.pop("created"))

        updated = isoparse(d.pop("updated"))

        def _parse_work_email(data: object) -> Union[None, Unset, str]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, str], data)

        work_email = _parse_work_email(d.pop("work_email", UNSET))

        sponsor = cls(
            id=id,
            feide_id=feide_id,
            first_name=first_name,
            last_name=last_name,
            orgunits=orgunits,
            created=created,
            updated=updated,
            work_email=work_email,
        )

        sponsor.additional_properties = d
        return sponsor

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
