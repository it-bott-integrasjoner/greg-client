import datetime
from typing import Any, Dict, List, Type, TypeVar, Union, cast

from attrs import define as _attrs_define
from attrs import field as _attrs_field
from dateutil.parser import isoparse

from ..types import UNSET, Unset

T = TypeVar("T", bound="RoleWrite")


@_attrs_define
class RoleWrite:
    """Serializer for use when writing.

    When writing we only want the id of the orgunit object.

        Attributes:
            id (int):
            end_date (datetime.date):
            sponsor (int):
            orgunit (int):
            created (datetime.datetime):
            updated (datetime.datetime):
            type (str):
            start_date (Union[None, Unset, datetime.date]):
            comments (Union[Unset, str]):
            available_in_search (Union[Unset, bool]):
            contact_person_unit (Union[Unset, str]):
    """

    id: int
    end_date: datetime.date
    sponsor: int
    orgunit: int
    created: datetime.datetime
    updated: datetime.datetime
    type: str
    start_date: Union[None, Unset, datetime.date] = UNSET
    comments: Union[Unset, str] = UNSET
    available_in_search: Union[Unset, bool] = UNSET
    contact_person_unit: Union[Unset, str] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        id = self.id

        end_date = self.end_date.isoformat()

        sponsor = self.sponsor

        orgunit = self.orgunit

        created = self.created.isoformat()

        updated = self.updated.isoformat()

        type = self.type

        start_date: Union[None, Unset, str]
        if isinstance(self.start_date, Unset):
            start_date = UNSET
        elif isinstance(self.start_date, datetime.date):
            start_date = self.start_date.isoformat()
        else:
            start_date = self.start_date

        comments = self.comments

        available_in_search = self.available_in_search

        contact_person_unit = self.contact_person_unit

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "id": id,
                "end_date": end_date,
                "sponsor": sponsor,
                "orgunit": orgunit,
                "created": created,
                "updated": updated,
                "type": type,
            }
        )
        if start_date is not UNSET:
            field_dict["start_date"] = start_date
        if comments is not UNSET:
            field_dict["comments"] = comments
        if available_in_search is not UNSET:
            field_dict["available_in_search"] = available_in_search
        if contact_person_unit is not UNSET:
            field_dict["contact_person_unit"] = contact_person_unit

        return field_dict

    def to_multipart(self) -> Dict[str, Any]:
        id = self.id if isinstance(self.id, Unset) else (None, str(self.id).encode(), "text/plain")

        end_date = self.end_date.isoformat().encode()

        sponsor = self.sponsor if isinstance(self.sponsor, Unset) else (None, str(self.sponsor).encode(), "text/plain")

        orgunit = self.orgunit if isinstance(self.orgunit, Unset) else (None, str(self.orgunit).encode(), "text/plain")

        created = self.created.isoformat().encode()

        updated = self.updated.isoformat().encode()

        type = self.type if isinstance(self.type, Unset) else (None, str(self.type).encode(), "text/plain")

        start_date: Union[None, Unset, datetime.date]
        if isinstance(self.start_date, Unset):
            start_date = UNSET
        elif isinstance(self.start_date, datetime.date):
            start_date = self.start_date.isoformat().encode()
        else:
            start_date = self.start_date

        comments = (
            self.comments if isinstance(self.comments, Unset) else (None, str(self.comments).encode(), "text/plain")
        )

        available_in_search = (
            self.available_in_search
            if isinstance(self.available_in_search, Unset)
            else (None, str(self.available_in_search).encode(), "text/plain")
        )

        contact_person_unit = (
            self.contact_person_unit
            if isinstance(self.contact_person_unit, Unset)
            else (None, str(self.contact_person_unit).encode(), "text/plain")
        )

        field_dict: Dict[str, Any] = {}
        field_dict.update(
            {key: (None, str(value).encode(), "text/plain") for key, value in self.additional_properties.items()}
        )
        field_dict.update(
            {
                "id": id,
                "end_date": end_date,
                "sponsor": sponsor,
                "orgunit": orgunit,
                "created": created,
                "updated": updated,
                "type": type,
            }
        )
        if start_date is not UNSET:
            field_dict["start_date"] = start_date
        if comments is not UNSET:
            field_dict["comments"] = comments
        if available_in_search is not UNSET:
            field_dict["available_in_search"] = available_in_search
        if contact_person_unit is not UNSET:
            field_dict["contact_person_unit"] = contact_person_unit

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        d = src_dict.copy()
        id = d.pop("id")

        end_date = isoparse(d.pop("end_date")).date()

        sponsor = d.pop("sponsor")

        orgunit = d.pop("orgunit")

        created = isoparse(d.pop("created"))

        updated = isoparse(d.pop("updated"))

        type = d.pop("type")

        def _parse_start_date(data: object) -> Union[None, Unset, datetime.date]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, str):
                    raise TypeError()
                start_date_type_0 = isoparse(data).date()

                return start_date_type_0
            except:  # noqa: E722
                pass
            return cast(Union[None, Unset, datetime.date], data)

        start_date = _parse_start_date(d.pop("start_date", UNSET))

        comments = d.pop("comments", UNSET)

        available_in_search = d.pop("available_in_search", UNSET)

        contact_person_unit = d.pop("contact_person_unit", UNSET)

        role_write = cls(
            id=id,
            end_date=end_date,
            sponsor=sponsor,
            orgunit=orgunit,
            created=created,
            updated=updated,
            type=type,
            start_date=start_date,
            comments=comments,
            available_in_search=available_in_search,
            contact_person_unit=contact_person_unit,
        )

        role_write.additional_properties = d
        return role_write

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
