import datetime
from typing import Any, Dict, List, Type, TypeVar, Union, cast

from attrs import define as _attrs_define
from attrs import field as _attrs_field
from dateutil.parser import isoparse

from ..types import UNSET, Unset

T = TypeVar("T", bound="PatchedRoleWrite")


@_attrs_define
class PatchedRoleWrite:
    """Serializer for use when writing.

    When writing we only want the id of the orgunit object.

        Attributes:
            id (Union[Unset, int]):
            start_date (Union[None, Unset, datetime.date]):
            end_date (Union[Unset, datetime.date]):
            sponsor (Union[Unset, int]):
            orgunit (Union[Unset, int]):
            created (Union[Unset, datetime.datetime]):
            updated (Union[Unset, datetime.datetime]):
            type (Union[Unset, str]):
            comments (Union[Unset, str]):
            available_in_search (Union[Unset, bool]):
            contact_person_unit (Union[Unset, str]):
    """

    id: Union[Unset, int] = UNSET
    start_date: Union[None, Unset, datetime.date] = UNSET
    end_date: Union[Unset, datetime.date] = UNSET
    sponsor: Union[Unset, int] = UNSET
    orgunit: Union[Unset, int] = UNSET
    created: Union[Unset, datetime.datetime] = UNSET
    updated: Union[Unset, datetime.datetime] = UNSET
    type: Union[Unset, str] = UNSET
    comments: Union[Unset, str] = UNSET
    available_in_search: Union[Unset, bool] = UNSET
    contact_person_unit: Union[Unset, str] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        id = self.id

        start_date: Union[None, Unset, str]
        if isinstance(self.start_date, Unset):
            start_date = UNSET
        elif isinstance(self.start_date, datetime.date):
            start_date = self.start_date.isoformat()
        else:
            start_date = self.start_date

        end_date: Union[Unset, str] = UNSET
        if not isinstance(self.end_date, Unset):
            end_date = self.end_date.isoformat()

        sponsor = self.sponsor

        orgunit = self.orgunit

        created: Union[Unset, str] = UNSET
        if not isinstance(self.created, Unset):
            created = self.created.isoformat()

        updated: Union[Unset, str] = UNSET
        if not isinstance(self.updated, Unset):
            updated = self.updated.isoformat()

        type = self.type

        comments = self.comments

        available_in_search = self.available_in_search

        contact_person_unit = self.contact_person_unit

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update({})
        if id is not UNSET:
            field_dict["id"] = id
        if start_date is not UNSET:
            field_dict["start_date"] = start_date
        if end_date is not UNSET:
            field_dict["end_date"] = end_date
        if sponsor is not UNSET:
            field_dict["sponsor"] = sponsor
        if orgunit is not UNSET:
            field_dict["orgunit"] = orgunit
        if created is not UNSET:
            field_dict["created"] = created
        if updated is not UNSET:
            field_dict["updated"] = updated
        if type is not UNSET:
            field_dict["type"] = type
        if comments is not UNSET:
            field_dict["comments"] = comments
        if available_in_search is not UNSET:
            field_dict["available_in_search"] = available_in_search
        if contact_person_unit is not UNSET:
            field_dict["contact_person_unit"] = contact_person_unit

        return field_dict

    def to_multipart(self) -> Dict[str, Any]:
        id = self.id if isinstance(self.id, Unset) else (None, str(self.id).encode(), "text/plain")

        start_date: Union[None, Unset, datetime.date]
        if isinstance(self.start_date, Unset):
            start_date = UNSET
        elif isinstance(self.start_date, datetime.date):
            start_date = self.start_date.isoformat().encode()
        else:
            start_date = self.start_date

        end_date: Union[Unset, bytes] = UNSET
        if not isinstance(self.end_date, Unset):
            end_date = self.end_date.isoformat().encode()

        sponsor = self.sponsor if isinstance(self.sponsor, Unset) else (None, str(self.sponsor).encode(), "text/plain")

        orgunit = self.orgunit if isinstance(self.orgunit, Unset) else (None, str(self.orgunit).encode(), "text/plain")

        created: Union[Unset, bytes] = UNSET
        if not isinstance(self.created, Unset):
            created = self.created.isoformat().encode()

        updated: Union[Unset, bytes] = UNSET
        if not isinstance(self.updated, Unset):
            updated = self.updated.isoformat().encode()

        type = self.type if isinstance(self.type, Unset) else (None, str(self.type).encode(), "text/plain")

        comments = (
            self.comments if isinstance(self.comments, Unset) else (None, str(self.comments).encode(), "text/plain")
        )

        available_in_search = (
            self.available_in_search
            if isinstance(self.available_in_search, Unset)
            else (None, str(self.available_in_search).encode(), "text/plain")
        )

        contact_person_unit = (
            self.contact_person_unit
            if isinstance(self.contact_person_unit, Unset)
            else (None, str(self.contact_person_unit).encode(), "text/plain")
        )

        field_dict: Dict[str, Any] = {}
        field_dict.update(
            {key: (None, str(value).encode(), "text/plain") for key, value in self.additional_properties.items()}
        )
        field_dict.update({})
        if id is not UNSET:
            field_dict["id"] = id
        if start_date is not UNSET:
            field_dict["start_date"] = start_date
        if end_date is not UNSET:
            field_dict["end_date"] = end_date
        if sponsor is not UNSET:
            field_dict["sponsor"] = sponsor
        if orgunit is not UNSET:
            field_dict["orgunit"] = orgunit
        if created is not UNSET:
            field_dict["created"] = created
        if updated is not UNSET:
            field_dict["updated"] = updated
        if type is not UNSET:
            field_dict["type"] = type
        if comments is not UNSET:
            field_dict["comments"] = comments
        if available_in_search is not UNSET:
            field_dict["available_in_search"] = available_in_search
        if contact_person_unit is not UNSET:
            field_dict["contact_person_unit"] = contact_person_unit

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        d = src_dict.copy()
        id = d.pop("id", UNSET)

        def _parse_start_date(data: object) -> Union[None, Unset, datetime.date]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, str):
                    raise TypeError()
                start_date_type_0 = isoparse(data).date()

                return start_date_type_0
            except:  # noqa: E722
                pass
            return cast(Union[None, Unset, datetime.date], data)

        start_date = _parse_start_date(d.pop("start_date", UNSET))

        _end_date = d.pop("end_date", UNSET)
        end_date: Union[Unset, datetime.date]
        if isinstance(_end_date, Unset):
            end_date = UNSET
        else:
            end_date = isoparse(_end_date).date()

        sponsor = d.pop("sponsor", UNSET)

        orgunit = d.pop("orgunit", UNSET)

        _created = d.pop("created", UNSET)
        created: Union[Unset, datetime.datetime]
        if isinstance(_created, Unset):
            created = UNSET
        else:
            created = isoparse(_created)

        _updated = d.pop("updated", UNSET)
        updated: Union[Unset, datetime.datetime]
        if isinstance(_updated, Unset):
            updated = UNSET
        else:
            updated = isoparse(_updated)

        type = d.pop("type", UNSET)

        comments = d.pop("comments", UNSET)

        available_in_search = d.pop("available_in_search", UNSET)

        contact_person_unit = d.pop("contact_person_unit", UNSET)

        patched_role_write = cls(
            id=id,
            start_date=start_date,
            end_date=end_date,
            sponsor=sponsor,
            orgunit=orgunit,
            created=created,
            updated=updated,
            type=type,
            comments=comments,
            available_in_search=available_in_search,
            contact_person_unit=contact_person_unit,
        )

        patched_role_write.additional_properties = d
        return patched_role_write

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
