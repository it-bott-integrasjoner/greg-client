import datetime
from typing import Any, Dict, List, Type, TypeVar, Union, cast

from attrs import define as _attrs_define
from attrs import field as _attrs_field
from dateutil.parser import isoparse

from ..types import UNSET, Unset

T = TypeVar("T", bound="PatchedRoleType")


@_attrs_define
class PatchedRoleType:
    """
    Attributes:
        id (Union[Unset, int]):
        created (Union[Unset, datetime.datetime]):
        updated (Union[Unset, datetime.datetime]):
        identifier (Union[Unset, str]):
        name_nb (Union[Unset, str]):
        name_en (Union[Unset, str]):
        description_nb (Union[Unset, str]):
        description_en (Union[Unset, str]):
        default_duration_days (Union[None, Unset, int]):
        max_days (Union[Unset, int]):
    """

    id: Union[Unset, int] = UNSET
    created: Union[Unset, datetime.datetime] = UNSET
    updated: Union[Unset, datetime.datetime] = UNSET
    identifier: Union[Unset, str] = UNSET
    name_nb: Union[Unset, str] = UNSET
    name_en: Union[Unset, str] = UNSET
    description_nb: Union[Unset, str] = UNSET
    description_en: Union[Unset, str] = UNSET
    default_duration_days: Union[None, Unset, int] = UNSET
    max_days: Union[Unset, int] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        id = self.id

        created: Union[Unset, str] = UNSET
        if not isinstance(self.created, Unset):
            created = self.created.isoformat()

        updated: Union[Unset, str] = UNSET
        if not isinstance(self.updated, Unset):
            updated = self.updated.isoformat()

        identifier = self.identifier

        name_nb = self.name_nb

        name_en = self.name_en

        description_nb = self.description_nb

        description_en = self.description_en

        default_duration_days: Union[None, Unset, int]
        if isinstance(self.default_duration_days, Unset):
            default_duration_days = UNSET
        else:
            default_duration_days = self.default_duration_days

        max_days = self.max_days

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update({})
        if id is not UNSET:
            field_dict["id"] = id
        if created is not UNSET:
            field_dict["created"] = created
        if updated is not UNSET:
            field_dict["updated"] = updated
        if identifier is not UNSET:
            field_dict["identifier"] = identifier
        if name_nb is not UNSET:
            field_dict["name_nb"] = name_nb
        if name_en is not UNSET:
            field_dict["name_en"] = name_en
        if description_nb is not UNSET:
            field_dict["description_nb"] = description_nb
        if description_en is not UNSET:
            field_dict["description_en"] = description_en
        if default_duration_days is not UNSET:
            field_dict["default_duration_days"] = default_duration_days
        if max_days is not UNSET:
            field_dict["max_days"] = max_days

        return field_dict

    def to_multipart(self) -> Dict[str, Any]:
        id = self.id if isinstance(self.id, Unset) else (None, str(self.id).encode(), "text/plain")

        created: Union[Unset, bytes] = UNSET
        if not isinstance(self.created, Unset):
            created = self.created.isoformat().encode()

        updated: Union[Unset, bytes] = UNSET
        if not isinstance(self.updated, Unset):
            updated = self.updated.isoformat().encode()

        identifier = (
            self.identifier
            if isinstance(self.identifier, Unset)
            else (None, str(self.identifier).encode(), "text/plain")
        )

        name_nb = self.name_nb if isinstance(self.name_nb, Unset) else (None, str(self.name_nb).encode(), "text/plain")

        name_en = self.name_en if isinstance(self.name_en, Unset) else (None, str(self.name_en).encode(), "text/plain")

        description_nb = (
            self.description_nb
            if isinstance(self.description_nb, Unset)
            else (None, str(self.description_nb).encode(), "text/plain")
        )

        description_en = (
            self.description_en
            if isinstance(self.description_en, Unset)
            else (None, str(self.description_en).encode(), "text/plain")
        )

        default_duration_days: Union[None, Unset, int]
        if isinstance(self.default_duration_days, Unset):
            default_duration_days = UNSET
        else:
            default_duration_days = self.default_duration_days

        max_days = (
            self.max_days if isinstance(self.max_days, Unset) else (None, str(self.max_days).encode(), "text/plain")
        )

        field_dict: Dict[str, Any] = {}
        field_dict.update(
            {key: (None, str(value).encode(), "text/plain") for key, value in self.additional_properties.items()}
        )
        field_dict.update({})
        if id is not UNSET:
            field_dict["id"] = id
        if created is not UNSET:
            field_dict["created"] = created
        if updated is not UNSET:
            field_dict["updated"] = updated
        if identifier is not UNSET:
            field_dict["identifier"] = identifier
        if name_nb is not UNSET:
            field_dict["name_nb"] = name_nb
        if name_en is not UNSET:
            field_dict["name_en"] = name_en
        if description_nb is not UNSET:
            field_dict["description_nb"] = description_nb
        if description_en is not UNSET:
            field_dict["description_en"] = description_en
        if default_duration_days is not UNSET:
            field_dict["default_duration_days"] = default_duration_days
        if max_days is not UNSET:
            field_dict["max_days"] = max_days

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        d = src_dict.copy()
        id = d.pop("id", UNSET)

        _created = d.pop("created", UNSET)
        created: Union[Unset, datetime.datetime]
        if isinstance(_created, Unset):
            created = UNSET
        else:
            created = isoparse(_created)

        _updated = d.pop("updated", UNSET)
        updated: Union[Unset, datetime.datetime]
        if isinstance(_updated, Unset):
            updated = UNSET
        else:
            updated = isoparse(_updated)

        identifier = d.pop("identifier", UNSET)

        name_nb = d.pop("name_nb", UNSET)

        name_en = d.pop("name_en", UNSET)

        description_nb = d.pop("description_nb", UNSET)

        description_en = d.pop("description_en", UNSET)

        def _parse_default_duration_days(data: object) -> Union[None, Unset, int]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, int], data)

        default_duration_days = _parse_default_duration_days(d.pop("default_duration_days", UNSET))

        max_days = d.pop("max_days", UNSET)

        patched_role_type = cls(
            id=id,
            created=created,
            updated=updated,
            identifier=identifier,
            name_nb=name_nb,
            name_en=name_en,
            description_nb=description_nb,
            description_en=description_en,
            default_duration_days=default_duration_days,
            max_days=max_days,
        )

        patched_role_type.additional_properties = d
        return patched_role_type

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
