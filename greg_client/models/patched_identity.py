import datetime
from typing import Any, Dict, List, Tuple, Type, TypeVar, Union, cast

from attrs import define as _attrs_define
from attrs import field as _attrs_field
from dateutil.parser import isoparse

from ..models.blank_enum import BlankEnum
from ..models.type_enum import TypeEnum
from ..models.verified_enum import VerifiedEnum
from ..types import UNSET, Unset

T = TypeVar("T", bound="PatchedIdentity")


@_attrs_define
class PatchedIdentity:
    """
    Attributes:
        id (Union[Unset, int]):
        created (Union[Unset, datetime.datetime]):
        updated (Union[Unset, datetime.datetime]):
        type (Union[Unset, TypeEnum]):
        source (Union[Unset, str]):
        value (Union[Unset, str]):
        verified (Union[BlankEnum, Unset, VerifiedEnum]):
        verified_at (Union[None, Unset, datetime.datetime]):
        invalid (Union[None, Unset, bool]):
        person (Union[Unset, int]):
        verified_by (Union[None, Unset, int]):
    """

    id: Union[Unset, int] = UNSET
    created: Union[Unset, datetime.datetime] = UNSET
    updated: Union[Unset, datetime.datetime] = UNSET
    type: Union[Unset, TypeEnum] = UNSET
    source: Union[Unset, str] = UNSET
    value: Union[Unset, str] = UNSET
    verified: Union[BlankEnum, Unset, VerifiedEnum] = UNSET
    verified_at: Union[None, Unset, datetime.datetime] = UNSET
    invalid: Union[None, Unset, bool] = UNSET
    person: Union[Unset, int] = UNSET
    verified_by: Union[None, Unset, int] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        id = self.id

        created: Union[Unset, str] = UNSET
        if not isinstance(self.created, Unset):
            created = self.created.isoformat()

        updated: Union[Unset, str] = UNSET
        if not isinstance(self.updated, Unset):
            updated = self.updated.isoformat()

        type: Union[Unset, str] = UNSET
        if not isinstance(self.type, Unset):
            type = self.type.value

        source = self.source

        value = self.value

        verified: Union[Unset, str]
        if isinstance(self.verified, Unset):
            verified = UNSET
        elif isinstance(self.verified, VerifiedEnum):
            verified = self.verified.value
        else:
            verified = self.verified.value

        verified_at: Union[None, Unset, str]
        if isinstance(self.verified_at, Unset):
            verified_at = UNSET
        elif isinstance(self.verified_at, datetime.datetime):
            verified_at = self.verified_at.isoformat()
        else:
            verified_at = self.verified_at

        invalid: Union[None, Unset, bool]
        if isinstance(self.invalid, Unset):
            invalid = UNSET
        else:
            invalid = self.invalid

        person = self.person

        verified_by: Union[None, Unset, int]
        if isinstance(self.verified_by, Unset):
            verified_by = UNSET
        else:
            verified_by = self.verified_by

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update({})
        if id is not UNSET:
            field_dict["id"] = id
        if created is not UNSET:
            field_dict["created"] = created
        if updated is not UNSET:
            field_dict["updated"] = updated
        if type is not UNSET:
            field_dict["type"] = type
        if source is not UNSET:
            field_dict["source"] = source
        if value is not UNSET:
            field_dict["value"] = value
        if verified is not UNSET:
            field_dict["verified"] = verified
        if verified_at is not UNSET:
            field_dict["verified_at"] = verified_at
        if invalid is not UNSET:
            field_dict["invalid"] = invalid
        if person is not UNSET:
            field_dict["person"] = person
        if verified_by is not UNSET:
            field_dict["verified_by"] = verified_by

        return field_dict

    def to_multipart(self) -> Dict[str, Any]:
        id = self.id if isinstance(self.id, Unset) else (None, str(self.id).encode(), "text/plain")

        created: Union[Unset, bytes] = UNSET
        if not isinstance(self.created, Unset):
            created = self.created.isoformat().encode()

        updated: Union[Unset, bytes] = UNSET
        if not isinstance(self.updated, Unset):
            updated = self.updated.isoformat().encode()

        type: Union[Unset, Tuple[None, bytes, str]] = UNSET
        if not isinstance(self.type, Unset):
            type = (None, str(self.type.value).encode(), "text/plain")

        source = self.source if isinstance(self.source, Unset) else (None, str(self.source).encode(), "text/plain")

        value = self.value if isinstance(self.value, Unset) else (None, str(self.value).encode(), "text/plain")

        verified: Union[BlankEnum, Unset, VerifiedEnum]
        if isinstance(self.verified, Unset):
            verified = UNSET
        elif isinstance(self.verified, VerifiedEnum):
            verified = (None, str(self.verified.value).encode(), "text/plain")
        else:
            verified = (None, str(self.verified.value).encode(), "text/plain")

        verified_at: Union[None, Unset, datetime.datetime]
        if isinstance(self.verified_at, Unset):
            verified_at = UNSET
        elif isinstance(self.verified_at, datetime.datetime):
            verified_at = self.verified_at.isoformat().encode()
        else:
            verified_at = self.verified_at

        invalid: Union[None, Unset, bool]
        if isinstance(self.invalid, Unset):
            invalid = UNSET
        else:
            invalid = self.invalid

        person = self.person if isinstance(self.person, Unset) else (None, str(self.person).encode(), "text/plain")

        verified_by: Union[None, Unset, int]
        if isinstance(self.verified_by, Unset):
            verified_by = UNSET
        else:
            verified_by = self.verified_by

        field_dict: Dict[str, Any] = {}
        field_dict.update(
            {key: (None, str(value).encode(), "text/plain") for key, value in self.additional_properties.items()}
        )
        field_dict.update({})
        if id is not UNSET:
            field_dict["id"] = id
        if created is not UNSET:
            field_dict["created"] = created
        if updated is not UNSET:
            field_dict["updated"] = updated
        if type is not UNSET:
            field_dict["type"] = type
        if source is not UNSET:
            field_dict["source"] = source
        if value is not UNSET:
            field_dict["value"] = value
        if verified is not UNSET:
            field_dict["verified"] = verified
        if verified_at is not UNSET:
            field_dict["verified_at"] = verified_at
        if invalid is not UNSET:
            field_dict["invalid"] = invalid
        if person is not UNSET:
            field_dict["person"] = person
        if verified_by is not UNSET:
            field_dict["verified_by"] = verified_by

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        d = src_dict.copy()
        id = d.pop("id", UNSET)

        _created = d.pop("created", UNSET)
        created: Union[Unset, datetime.datetime]
        if isinstance(_created, Unset):
            created = UNSET
        else:
            created = isoparse(_created)

        _updated = d.pop("updated", UNSET)
        updated: Union[Unset, datetime.datetime]
        if isinstance(_updated, Unset):
            updated = UNSET
        else:
            updated = isoparse(_updated)

        _type = d.pop("type", UNSET)
        type: Union[Unset, TypeEnum]
        if isinstance(_type, Unset):
            type = UNSET
        else:
            type = TypeEnum(_type)

        source = d.pop("source", UNSET)

        value = d.pop("value", UNSET)

        def _parse_verified(data: object) -> Union[BlankEnum, Unset, VerifiedEnum]:
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, str):
                    raise TypeError()
                verified_type_0 = VerifiedEnum(data)

                return verified_type_0
            except:  # noqa: E722
                pass
            if not isinstance(data, str):
                raise TypeError()
            verified_type_1 = BlankEnum(data)

            return verified_type_1

        verified = _parse_verified(d.pop("verified", UNSET))

        def _parse_verified_at(data: object) -> Union[None, Unset, datetime.datetime]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, str):
                    raise TypeError()
                verified_at_type_0 = isoparse(data)

                return verified_at_type_0
            except:  # noqa: E722
                pass
            return cast(Union[None, Unset, datetime.datetime], data)

        verified_at = _parse_verified_at(d.pop("verified_at", UNSET))

        def _parse_invalid(data: object) -> Union[None, Unset, bool]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, bool], data)

        invalid = _parse_invalid(d.pop("invalid", UNSET))

        person = d.pop("person", UNSET)

        def _parse_verified_by(data: object) -> Union[None, Unset, int]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, int], data)

        verified_by = _parse_verified_by(d.pop("verified_by", UNSET))

        patched_identity = cls(
            id=id,
            created=created,
            updated=updated,
            type=type,
            source=source,
            value=value,
            verified=verified,
            verified_at=verified_at,
            invalid=invalid,
            person=person,
            verified_by=verified_by,
        )

        patched_identity.additional_properties = d
        return patched_identity

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
